import pytest

from app import app, db
from models import *
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

@pytest.fixture
def test_client():
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///mockdb.sqlite3"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS "] = False
    app.config['TESTING'] = True

    with app.test_client() as testing_client:
        with app.app_context():
            yield testing_client


@pytest.fixture
def init_database(test_client):
    db.create_all()

    # country data
    country1 = Country(
        country_id=1,
        name="United States",
        capital="Washington D.C.",
        gdp="21841644347",
        population=328454874,
        region="North America",
        latitude=37.0,
        longitude=-95.8,
        flag="usa.png",
        area=3797668.4,
        gini=41.9,
        income_level="High income"
    )
    db.session.add(country1)

    country2 = Country(
        country_id=2,
        name="Switzerland",
        capital="Bern",
        gdp="35681432511",
        population=85942371,
        region="Europe",
        latitude=46.8,
        longitude=8.2,
        flag="switzerland.png",
        area=15940.6,
        gini=33.1,
        income_level="Middle income"
    )
    db.session.add(country2)

    country3 = Country(
        country_id=3,
        name="Japan",
        capital="Tokyo",
        gdp="7496178207",
        population=126001472,
        region="Asia",
        latitude=36.2,
        longitude=138.2,
        flag="japan.png",
        area=145914.6,
        gini=29.7,
        income_level="High income"
    )
    db.session.add(country3)

    # climate data
    climate1 = Climate(
        country_id=1,
        country_name="United States",
        climate_zone="Humid subtropical, no dry season",
        ch4 = "1990:215468.1;1991:224867.2;",
        co2 = "1990:145674.3;1991:146704.4;",
        ghg = "1990:334789.5;1991:394144.6;",
        no2 = "1990:24798.7;1991:27095.8;",
        pm25 = "1990:45.9;1991:44.1;",
        temperature = "1990:56.2;1991:57.3;",
        rainfall = "1990:74.1;1991:75.2;",
        aqi = 2,
        pm10 = 18.5
    )
    db.session.add(climate1)

    climate2 = Climate(
        country_id=2,
        country_name="Switzerland",
        climate_zone="Marine west coast, warm summer",
        ch4 = "1990:75421.3;1991:74135.9;",
        co2 = "1990:66128.7;1991:67107.6;",
        ghg = "1990:101741.7;1991:109806.2;",
        no2 = "1990:10101.7;1991:10024.0;",
        pm25 = "1990:29.0;1991:28.5;",
        temperature = "1990:31.3;1991:31.9;",
        rainfall = "1990:27.2;1991:28.6;",
        aqi = 4,
        pm10 = 48.12
    )
    db.session.add(climate2)

    climate3 = Climate(
        country_id=3,
        country_name="Japan",
        climate_zone="Humid subtropical, no dry season",
        ch4 = "1990:62147.1;1991:61984.0;",
        co2 = "1990:56147.6;1991:57449.2;",
        ghg = "1990:98801.3;1991:98450.3;",
        no2 = "1990:20012.1;1991:20197.2;",
        pm25 = "1990:19.2;1991:19.5;",
        temperature = "1990:49.1;1991:48.7;",
        rainfall = "1990:101.1;1991:100.1;",
        aqi = 2,
        pm10 = 6.7
    )
    db.session.add(climate3)

    # news data
    news1 = News(
        news_id=1,
        title="No news in US",
        description="US is facing a sharp decline in news",
        content="There is no news today.",
        country_name="United States",
        date="2021-04-07T19:14:38",
        url="www.news.com/article/1",
        provider="news",
        language="english",
        thumbnail="image1.jpg",
        word_count=5
    )
    db.session.add(news1)

    news2 = News(
        news_id=2,
        title="Swiss Cheese",
        description="Rise in Swiss cheese sales",
        content="Swiss economy is now fully dependent on cheese sales.",
        country_name="Switzerland",
        date="2021-04-01T15:35:01",
        url="www.swissnews.com/article/15",
        provider="swissnews",
        language="english",
        thumbnail="image2.jpg",
        word_count=9
    )
    db.session.add(news2)

    news3 = News(
        news_id=3,
        title="Japan News",
        description="Interesting news in Japan",
        content="The news in Japan are interesting.",
        country_name="Japan",
        date="2021-04-2T09:20:00",
        url="www.japantimes.com/article/27",
        provider="japantimes",
        language="english",
        thumbnail="image3.jpg",
        word_count=6
    )
    db.session.add(news3)

    db.session.commit()

    yield

    db.drop_all()
