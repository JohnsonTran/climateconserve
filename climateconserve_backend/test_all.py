import json
import requests

# ----
# countries
# ----

def test_countries(test_client, init_database):
    res = test_client.get("/api/countries")
    data = json.loads(res.get_data().decode("utf-8"))
    assert res.status_code == 200
    assert len(data) == 3

def test_valid_country_id(test_client, init_database):
    res = test_client.get("/api/country/1")
    data = json.loads(res.get_data().decode("utf-8"))
    assert data["id"] == 1
    assert data["name"] == "United States"
    assert data["capital"] == "Washington D.C."
    assert data["region"] == "North America"
    assert data["area"] == 3797668.4
    assert data["population"] == 328454874
    assert data["latitude"] == 37.0
    assert data["longitude"] == -95.8
    assert data["gini"] == 41.9
    assert data["gdp"] == "21841644347"
    assert data["flag"] == "usa.png"
    assert data["income_level"] == "High income"

def test_invalid_country_id(test_client, init_database):
    res = test_client.get("/api/country/8651")
    assert res.data == b"Invalid Country ID"

def test_country_attributes(test_client, init_database):
    res = test_client.get("/api/country/2")
    data = json.loads(res.get_data().decode("utf-8"))
    assert "id" in data
    assert "name" in data
    assert "capital" in data
    assert "region" in data
    assert "area" in data
    assert "population" in data
    assert "latitude" in data
    assert "longitude" in data
    assert "gini" in data
    assert "gdp" in data
    assert "flag" in data
    assert "income_level" in data

# ----
# climates reports
# ----

def test_climate_reports(test_client, init_database):
    res = test_client.get("/api/climate-reports")
    data = json.loads(res.get_data().decode("utf-8"))
    assert res.status_code == 200
    assert len(data) == 3

def test_valid_climate_report_id(test_client, init_database):
    res = test_client.get("/api/climate-report/2")
    data = json.loads(res.get_data().decode("utf-8"))
    assert data["id"] == 2
    assert data["name"] == "Switzerland"
    assert data["climate_zone"] == "Marine west coast, warm summer"
    assert data["ch4"] == {"1990":"75421.3", "1991":"74135.9"}
    assert data["co2"] == {"1990":"66128.7", "1991":"67107.6"}
    assert data["ghg"] == {"1990":"101741.7", "1991":"109806.2"}
    assert data["no2"] == {"1990":"10101.7", "1991":"10024.0"}
    assert data["pm25"] == {"1990":"29.0", "1991":"28.5"}
    assert data["temperature"] == {"1990":"31.3", "1991":"31.9"}
    assert data["rainfall"] == {"1990":"27.2", "1991":"28.6"}
    assert data["pm10"] == 48.12
    assert data["aqi"] == 4

def test_invalid_climate_report_id(test_client, init_database):
    res = test_client.get("/api/climate-report/8651")
    assert res.data == b"Invalid Climate Reports ID"

def test_climate_attributes(test_client, init_database):
    res = test_client.get("/api/climate-report/3")
    data = json.loads(res.get_data().decode("utf-8"))
    assert "id" in data
    assert "name" in data
    assert "ch4" in data
    assert "co2" in data
    assert "ghg" in data
    assert "no2" in data
    assert "pm25" in data
    assert "temperature" in data
    assert "rainfall" in data
    assert "pm10" in data
    assert "aqi" in data
    assert "climate_zone" in data

# ----
# news
# ----

def test_news(test_client, init_database):
    res = test_client.get("/api/all-news/1")
    data = json.loads(res.get_data().decode("utf-8"))
    assert res.status_code == 200
    assert len(data[1:]) == 3

def test_news_valid_country_id(test_client, init_database):
    res = test_client.get("/api/news/1")
    data = json.loads(res.get_data().decode("utf-8"))
    for news in data:
        assert news["country"] == "United States"

def test_news_invalid_country_id(test_client, init_database):
    res = test_client.get("/api/news/8651")
    data = json.loads(res.get_data().decode("utf-8"))
    assert len(data) == 0

def test_valid_article_id(test_client, init_database):
    res = test_client.get("/api/article/3")
    data = json.loads(res.get_data().decode("utf-8"))
    assert data["article-id"] == 3
    assert data["country"] == "Japan"
    assert data["title"] == "Japan News"
    assert data["description"] == "Interesting news in Japan"
    assert data["date"] == "2021-04-2T09:20:00"
    assert data["url"] == "www.japantimes.com/article/27"
    assert data["content"] == "The news in Japan are interesting."
    assert data["provider"] == "japantimes"
    assert data["language"] == "english"
    assert data["thumbnail"] == "image3.jpg"
    assert data["word_count"] == 6

def test_invalid_article_id(test_client, init_database):
    res = test_client.get("/api/article/8561")
    assert res.data == b"Invalid Article ID"

def test_article_attributes(test_client, init_database):
    res = test_client.get("/api/article/1")
    data = json.loads(res.get_data().decode("utf-8"))
    assert "article-id" in data
    assert "country" in data
    assert "title" in data
    assert "description" in data
    assert "date" in data
    assert "url" in data
    assert "content" in data
    assert "provider" in data
    assert "language" in data
    assert "thumbnail" in data
    assert "word_count" in data
