from unittest import main, TestCase
import requests

class TestAPI(TestCase):
    # ----
    # countries
    # ----

    # test all countries
    def test1(self):
        r = requests.get("https://www.climateconserve.me/api/countries")
        data = r.json()
        assert r.status_code == 200
        assert len(data) == 193

    # test valid country id
    def test2(self):
        r = requests.get("https://www.climateconserve.me/api/country/185")
        data = r.json()
        assert data["name"] == "United States"
        assert data["capital"] == "Washington D.C."
        assert data["region"] == "North America"
        assert data["area"] == 9629090.0
        assert data["population"] == 328239523
        assert data["latitude"] == 38.8895
        assert data["longitude"] == -77.032
        assert data["gini"] == 48.0
        assert data["gdp"] == "2.14332E+13"
        assert data["flag"] == "https://restcountries.eu/data/usa.svg"

    # test invalid country id
    def test3(self):
        r = requests.get("https://www.climateconserve.me/api/country/500")
        assert r.text == "Invalid Country ID"

    # ----
    # climates reports
    # ----

    # test all climate reports
    def test4(self):
        r = requests.get("https://www.climateconserve.me/api/climate-reports")
        data = r.json()
        assert r.status_code == 200
        assert len(data) == 193

    # test valid climate report id
    def test5(self):
        r = requests.get("https://www.climateconserve.me/api/climate-report/185")
        data = r.json()
        assert data["name"] == "United States"
        assert len(data["ch4"]) == 23
        assert len(data["co2"]) == 27
        assert len(data["ghg"]) == 23
        assert len(data["no2"]) == 23
        assert len(data["pm25"]) == 28
        assert len(data["temperature"]) == 26
        assert len(data["rainfall"]) == 26

    # test invalid climate report id
    def test6(self):
        r = requests.get("https://www.climateconserve.me/api/climate-report/500")
        assert r.text == "Invalid Climate Reports ID"

    # ----
    # news
    # ----

    # test getting all news
    def test7(self):
        r = requests.get("https://www.climateconserve.me/api/all-news/1")
        data = r.json()
        assert len(data[1:]) == 20

    # test getting news by country with valid id
    def test8(self):
        r = requests.get("https://www.climateconserve.me/api/news/185")
        data = r.json()
        for news in data:
            assert news["country"] == "United States"

    # test getting news by country with invalid id
    def test9(self):
        r = requests.get("https://www.climateconserve.me/api/news/500")
        data = r.json()
        assert len(data) == 0

    # test valid article id
    def test10(self):
        r = requests.get("https://www.climateconserve.me/api/article/18")
        data = r.json()
        assert data["country"] != None
        assert data["title"] != None
        assert data["article-id"] != None
        assert data["description"] != None
        assert data["date"] != None
        assert data["url"] != None
        assert data["content"] != None
        assert data["provider"] != None
        assert data["language"] != None
        assert data["thumbnail"] != None

    # test invalid article id
    def test11(self):
        r = requests.get("https://www.climateconserve.me/api/article/1800")
        assert r.text == "Invalid Article ID"

# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()

