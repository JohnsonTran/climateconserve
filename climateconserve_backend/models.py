from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Country(db.Model):
    __tablename__ = "country"
    country_id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(256))
    capital = db.Column(db.String(256))
    gdp = db.Column(db.String(30))
    population = db.Column(db.Integer())
    region = db.Column(db.String(256))
    latitude = db.Column(db.Float())
    longitude = db.Column(db.Float())
    flag = db.Column(db.String(256))
    area = db.Column(db.Float())
    gini = db.Column(db.Float())
    income_level = db.Column(db.String(256))

class Climate(db.Model):
    __tablename__ = "climate"
    country_id = db.Column(db.Integer(), primary_key=True)
    country_name = db.Column(db.String(255))
    climate_zone = db.Column(db.String(255))
    ch4 = db.Column(db.String(550))             # 1990-2012
    co2 = db.Column(db.String(550))             # 1990-2016
    ghg = db.Column(db.String(550))             # 1990-2012
    no2 = db.Column(db.String(550))             # 1990-2012
    pm25 = db.Column(db.String(550))            # 2017
    temperature = db.Column(db.String(550))     # 1991-2016
    rainfall = db.Column(db.String(550))        # 1991-2016
    pm10 = db.Column(db.Float())
    aqi = db.Column(db.Integer())

class News(db.Model):
    __tablename__ = "news"
    news_id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String(500))
    description = db.Column(db.String(2000))
    content = db.Column(db.String(10000))
    country_name = db.Column(db.String(256))
    date = db.Column(db.String(30))
    url = db.Column(db.String(500))
    provider = db.Column(db.String(500))
    language = db.Column(db.String(50))
    thumbnail = db.Column(db.String(256))
    word_count = db.Column(db.Integer())
