import json
from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
# from setup import app, db
from models import *

#ADDED BY SEJAL
import math
from flask_paginate import Pagination, get_page_args

app = Flask(__name__)
# app.config.from_object('config')
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://admin:climateconserve20@climateconserve.cnxlrrdbtuam.us-east-2.rds.amazonaws.com:3306/climateconserve"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
CORS(app)
db.init_app(app)

# NOTE: This route is needed for the default EB health check route
@app.route('/')
def home():
    return "ok"

# used for db testing and debugging, delete later
@app.route('/api/delete_db')
def delete_db():
    db.drop_all()
    return "ok"

# used for db testing and debugging, delete later
@app.route('/api/add_db')
def add_db():
    db.create_all()
    with open('all.json', 'r') as a:
        all = json.load(a)

    with open('news.json', 'r') as a:
        news = json.load(a)
    
    i = 1
    for country in all:
        data = all[country]
        db.session.add(Country(country_id=i, name=country, capital=data["capital"], gdp=data["GDP"], \
                                population=data["population"], region=data["region"], \
                                latitude=data["latitude"], longitude=data["longitude"], \
                                flag=data["flag"], area=data["area"], gini=data["gini"], \
                                income_level=data["income_level"]))
        country_climate = Climate(country_id=i, country_name=country, pm10=all[country]["PM10"], aqi=all[country]["AQI"], climate_zone=all[country]["climate_zone"])
        attributes = ["rainfall", "temperature", "CH4", "CO2", "GHG", "NO2", "PM2.5"]
        attrib_dict = {}
        # encoding all of the ranges
        for attribute in attributes:
            encoded = ""
            for (year, data) in all[country][attribute].items():
                encoded += f"{year}:{data};"
            attrib_dict[attribute] = encoded

        country_climate.rainfall = attrib_dict["rainfall"]
        country_climate.temperature = attrib_dict["temperature"]
        country_climate.ch4 = attrib_dict["CH4"]
        country_climate.co2 = attrib_dict["CO2"]
        country_climate.ghg = attrib_dict["GHG"]
        country_climate.no2 = attrib_dict["NO2"]
        country_climate.pm25 = attrib_dict["PM2.5"]
        db.session.add(country_climate)
        i += 1
    
    for story in news:
        data = news[story]
        db.session.add(News(news_id=story, title=data["title"], description=data["description"], \
                        content=data["content"], country_name=data["country"], date=data["datePublished"], \
                        url=data["url"], provider=data["provider"], language=data["language"], \
                        thumbnail=data["thumbnail"], word_count=data["wordCount"]))

    db.session.commit()
    db.session.close()
    return "ok"

# helper function that converts the string ranges into a dict
def decode_ranges(encoded_string):
    data = encoded_string.split(";")
    # removes the empty string at the end
    data.pop()
    result = {}
    for pair in data:
        data = pair.split(":")
        year = int(data[0])
        # sometimes there is no value for a year
        try:
            val = float(data[1])
        except ValueError:
            val = None
        result[year] = val
    return result

#ADDED BY SEJAL

@app.route('/api/countries')
def countries():
    countries = Country.query.all()
    data = []
    for country in countries:
        data_dict = {"name": country.name, "id": country.country_id, "capital": country.capital, "gdp": country.gdp, \
                            "population": country.population, "region": country.region, \
                            "latitude": country.latitude, "longitude": country.longitude, \
                            "flag": country.flag, "area": country.area, "gini": country.gini, \
                            "income_level": country.income_level }
        data.append(data_dict)
    return jsonify(data)

@app.route('/api/country/<id>')
def country(id):
    country = Country.query.get(id)
    if country:
        return {"name": country.name, "id": country.country_id, "capital": country.capital, "gdp": country.gdp, \
                    "population": country.population, "region": country.region, \
                    "latitude": country.latitude, "longitude": country.longitude, \
                    "flag": country.flag, "area": country.area, "gini": country.gini, \
                    "income_level": country.income_level }
    return "Invalid Country ID"

@app.route('/api/climate-reports')
def climate_reports():
    countries = Climate.query.all()
    data = []
    for country in countries:
        data_dict = {"name": country.country_name, "id": country.country_id, "ch4": split_string(country.ch4), "co2": split_string(country.co2), \
                        "ghg": split_string(country.ghg), "no2": split_string(country.no2), \
                        "pm25": split_string(country.pm25), "temperature": split_string(country.temperature), \
                        "rainfall": split_string(country.rainfall), "pm10": country.pm10, "aqi": country.aqi, \
                        "climate_zone": country.climate_zone }
        data.append(data_dict)
    return jsonify(data)

@app.route('/api/climate-report/<id>')
def climate_report(id):
    country = Climate.query.get(id)
    if country:
        return {"name": country.country_name, "id": country.country_id, "ch4": split_string(country.ch4), "co2": split_string(country.co2), \
                "ghg": split_string(country.ghg), "no2": split_string(country.no2), \
                "pm25": split_string(country.pm25), "temperature": split_string(country.temperature), \
                "rainfall": split_string(country.rainfall), "pm10": country.pm10, "aqi": country.aqi, \
                "climate_zone": country.climate_zone }
    return "Invalid Climate Reports ID"

@app.route('/api/all-news/<int:page>')
def all_news(page):
    result = []
    current_page = int(page)
    per_page = 20
    news = News.query.all()
    news_items = News.query.paginate(page, per_page).items

    total = 0
    for item in news:
        total+=1

    info = {}
    info["current-page"] = current_page
    info["total-pages"] = math.ceil(total/per_page)
    info["total-articles"] = total
    result.append(info)

    for url in news_items:
        data_dict = {"country": url.country_name, "title": url.title, "article-id": url.news_id, "description": url.description, \
                            "date": url.date, "url": url.url, "content": url.content, \
                            "provider": url.provider, "language": url.language, \
                            "thumbnail": url.thumbnail, "word_count": url.word_count }
        result.append(data_dict)
    return jsonify(result)

# gets article by country id
@app.route('/api/news/<id>')
def news(id):
    country = Country.query.get(id)
    
    results = []
    if country:
        news = News.query.filter(News.country_name==country.name)
        for url in news:
            data_dict = {"country": url.country_name, "title": url.title, "article-id": url.news_id, 
                            "description": url.description, "date": url.date, "url": url.url, \
                            "content": url.content, "provider": url.provider, "language": url.language, \
                            "thumbnail": url.thumbnail, "word_count": url.word_count }
            results.append(data_dict)
    return jsonify(results)

# gets article by id
@app.route('/api/article/<id>')
def article(id):
    url = News.query.get(id)
    if url:
        return {"country": url.country_name, "country_id": id, "title": url.title, 
                    "article-id": url.news_id, "description": url.description, \
                    "date": url.date, "url": url.url, "content": url.content, \
                    "provider": url.provider, "language": url.language, \
                    "thumbnail": url.thumbnail, "word_count": url.word_count }

    return "Invalid Article ID"

def split_string(s):
    result = {}
    years = s.split(";")
    for entry in years:
        entry_split = entry.split(":")
        if(len(entry_split) == 2):
            year = entry_split[0]
            data = entry_split[1]
            result[year] = data
    return result

# search endpoints for each model that returns paginated results
@app.route('/api/search_country/<search_query>/<int:page>')
def search_country(search_query, page):
    result = []
    current_page = int(page)
    search_query = search_query.lower()

    per_page = 20
    country_items = Country.query.filter(Country.name.ilike(f"%{search_query}%") | \
        Country.capital.ilike(f"%{search_query}%") | Country.region.ilike(f"%{search_query}%") | \
        Country.income_level.ilike(f"%{search_query}%"))

    info = {}
    info["current-page"] = current_page
    info["total-countries"] = country_items.count()
    info["total-pages"] = math.ceil(country_items.count()/per_page)

    if country_items.count() == 0 or page > math.ceil(country_items.count()/per_page):
        return jsonify([])

    result.append(info)

    for country in country_items.paginate(page, per_page).items:
        data_dict = {"name": country.name, "id": country.country_id, "capital": country.capital, "gdp": country.gdp, \
            "population": country.population, "region": country.region, \
            "latitude": country.latitude, "longitude": country.longitude, \
            "flag": country.flag, "area": country.area, "gini": country.gini, \
            "income_level": country.income_level }
        result.append(data_dict)

    return jsonify(result)

@app.route('/api/search_climate/<search_query>/<int:page>')
def search_climate(search_query, page):
    result = []
    current_page = int(page)
    search_query = search_query.lower()

    per_page = 20
    climate_items = Climate.query.filter(Climate.country_name.ilike(f"%{search_query}%") | \
        Climate.climate_zone.ilike(f"%{search_query}%"))

    info = {}
    info["current-page"] = current_page
    info["total-climates"] = climate_items.count()
    info["total-pages"] = math.ceil(climate_items.count()/per_page)

    if climate_items.count() == 0 or page > math.ceil(climate_items.count()/per_page):
        return jsonify([])

    result.append(info)

    for climate in climate_items.paginate(page, per_page).items:
        data_dict = {"name": climate.country_name, "id": climate.country_id, "ch4": split_string(climate.ch4), "co2": split_string(climate.co2), \
            "ghg": split_string(climate.ghg), "no2": split_string(climate.no2), \
            "pm25": split_string(climate.pm25), "temperature": split_string(climate.temperature), \
            "rainfall": split_string(climate.rainfall), "pm10": climate.pm10, "aqi": climate.aqi, \
            "climate_zone": climate.climate_zone }
        result.append(data_dict)

    return jsonify(result)

@app.route('/api/search_news/<search_query>/<int:page>')
def search_news(search_query, page):
    result = []
    current_page = int(page)
    search_query = search_query.lower()

    per_page = 20
    news_items = News.query.filter(News.country_name.ilike(f"%{search_query}%") | \
        News.title.ilike(f"%{search_query}%") | News.description.ilike(f"%{search_query}%") | \
        News.date.ilike(f"%{search_query}%") | \
        News.provider.ilike(f"%{search_query}%"))

    info = {}
    info["current-page"] = current_page
    info["total-articles"] = news_items.count()
    info["total-pages"] = math.ceil(news_items.count()/per_page)

    if news_items.count() == 0 or page > math.ceil(news_items.count()/per_page):
        return jsonify([])

    result.append(info)

    for article in news_items.paginate(page, per_page).items:
        data_dict = {"country": article.country_name, "title": article.title, "article-id": article.news_id, "description": article.description, \
                    "date": article.date, "url": article.url, "content": article.content, \
                    "provider": article.provider, "language": article.language, \
                    "thumbnail": article.thumbnail, "word_count": article.word_count }
        result.append(data_dict)

    return jsonify(result)


if __name__ == '__main__':
    app.run(port=8080)