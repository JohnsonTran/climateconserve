echo "Deploying Backend..."
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 322723416243.dkr.ecr.us-east-2.amazonaws.com
docker build -t climateconserve-backend .
docker tag climateconserve-backend:latest 322723416243.dkr.ecr.us-east-2.amazonaws.com/climateconserve1-backend:latest
docker push 322723416243.dkr.ecr.us-east-2.amazonaws.com/climateconserve1-backend:latest
cd aws_deploy
eb deploy