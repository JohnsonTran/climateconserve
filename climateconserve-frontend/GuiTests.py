from selenium import webdriver
import requests


# Will consider turning the tests into seperate functions later on. For now, this iterates through page links and checks things such as
# if certain text is on the page, if the images aren't broken, if the links aren't broken etc.

PATH = "/Users/jdogtherock/chromedriver"

driver = webdriver.Chrome(PATH)
driver.implicitly_wait(20)
driver.get("https://www.climateconserve.me/")
driver.maximize_window()


# Checks if we arrive at the correct url and the title of the webpage is correct.
print("At 'Home' page.")
assert driver.title == "Climate Conserve"
assert driver.current_url == "https://www.climateconserve.me/"

# HOME PAGE CHECKS

# Checks for broken links
links = driver.find_elements_by_css_selector("a")
for link in links:
    r = requests.head(link.get_attribute("href"))
    print(link.get_attribute("href"), r.status_code)
    assert r.status_code > 199 and r.status_code < 300

# Checks for broken images
images = driver.find_elements_by_tag_name("img")
for img in images:
    response = requests.get(img.get_attribute("src"), stream=True)
    print(response.status_code)
    assert response.status_code > 199 and response.status_code < 300

# Checks for nav bar
print("Checking if nav bar is present")
nav = driver.find_elements_by_tag_name("nav")
print(len(nav))
assert len(nav) == 1

# ABOUT PAGE CHECKS

# Navigate to about page
print("Going to 'About' Page")
driver.find_element_by_link_text("About").click()
driver.implicitly_wait(20)

# Checks for broken links
links = driver.find_elements_by_css_selector("a")
for link in links:
    r = requests.head(link.get_attribute("href"))
    print(link.get_attribute("href"), r.status_code)
    assert r.status_code > 199 and r.status_code < 300

# Checks for broken images
images = driver.find_elements_by_tag_name("img")
for img in images:
    response = requests.get(img.get_attribute("src"), stream=True)
    print(response.status_code)
    assert response.status_code > 199 and response.status_code < 300

# Check for text that should be present
assert "About" in driver.page_source
assert "Vivek Chilakamarri" in driver.page_source
assert "Kevin Dao" in driver.page_source
assert "Jeffrey Gordon" in driver.page_source
assert "Sejal Sharma" in driver.page_source
assert "Johnson Tran" in driver.page_source

# Nav bar check
print("Checking if nav bar is present")
nav = driver.find_elements_by_tag_name("nav")
print(len(nav))
assert len(nav) == 1

# COUNTRIES PAGE CHECKS

# Navigate to countries page
print("Going to 'Countries' Page")
driver.find_element_by_link_text("Countries").click()
driver.implicitly_wait(20)

# Nav bar check
print("Checking if nav bar is present")
nav = driver.find_elements_by_tag_name("nav")
print(len(nav))
assert len(nav) == 1

# Link check
links = driver.find_elements_by_css_selector("a")
for link in links:
    r = requests.head(link.get_attribute("href"))
    print(link.get_attribute("href"), r.status_code)
    assert r.status_code > 199 and r.status_code < 300

# Text Check
assert "ID" in driver.page_source
assert "Country" in driver.page_source
assert "Region" in driver.page_source
assert "Population Size" in driver.page_source
assert "Gross Domestic Product (USD)" in driver.page_source
assert "Land Size" in driver.page_source

# Testing Search Bar
search_box = driver.find_element_by_xpath(
    "//*[@id='root']/div/div/div/div/div[1]/div[3]/div/input"
)
search_box.send_keys("United Kingdom")
driver.implicitly_wait(20)
assert "United Kingdom" in driver.page_source
assert "Europe &amp; Central Asia" in driver.page_source

# Click on UK Link and inspect the model
print("Going to 'United Kingdom' Page")
driver.find_element_by_link_text("United Kingdom").click()
driver.implicitly_wait(20)

# Image Check
images = driver.find_elements_by_tag_name("img")
for img in images:
    response = requests.get(img.get_attribute("src"), stream=True)
    print(response.status_code)
    assert response.status_code > 199 and response.status_code < 300

# Link Check
links = driver.find_elements_by_css_selector("a")
for link in links:
    r = requests.head(link.get_attribute("href"))
    print(link.get_attribute("href"), r.status_code)
    assert r.status_code > 199 and r.status_code < 300


assert "United Kingdom" in driver.page_source

# Nav bar check
nav = driver.find_elements_by_tag_name("nav")
print(len(nav))
assert len(nav) == 1

# Click into climate link
driver.find_element_by_link_text("Learn about the climate in United Kingdom").click()
driver.implicitly_wait(20)

# Nav check
nav = driver.find_elements_by_tag_name("nav")
print(len(nav))
assert len(nav) == 1

# Link check
links = driver.find_elements_by_css_selector("a")
for link in links:
    r = requests.head(link.get_attribute("href"))
    print(link.get_attribute("href"), r.status_code)
    assert r.status_code > 199 and r.status_code < 300

# Click into news page
driver.find_element_by_link_text("News").click()
driver.implicitly_wait(20)

assert "News" in driver.page_source

# Link check
links = driver.find_elements_by_css_selector("a")
for link in links:
    r = requests.head(link.get_attribute("href"))
    print(link.get_attribute("href"), r.status_code)
    assert r.status_code > 199 and r.status_code < 300

# Nav bar check
nav = driver.find_elements_by_tag_name("nav")
print(len(nav))
assert len(nav) == 1

# click into article 1
driver.find_element_by_xpath("//*[@id='root']/div/div/div/div/div[1]/div/div/a").click()
driver.implicitly_wait(20)

# Nav check
nav = driver.find_elements_by_tag_name("nav")
assert len(nav) == 1

# Link check
links = driver.find_elements_by_css_selector("a")
for link in links:
    r = requests.head(link.get_attribute("href"))
    print(link.get_attribute("href"), r.status_code)
    assert r.status_code > 199 and r.status_code < 300

assert "Afghanistan- An Ounce of Pandemic Prevention" in driver.page_source

# CLIMATES PAGE CHECKS

driver.find_element_by_link_text("Climates").click()
driver.implicitly_wait(20)

# Nav check
print("Checking if nav bar is present")
nav = driver.find_elements_by_tag_name("nav")
print(len(nav))
assert len(nav) == 1

# Link check
links = driver.find_elements_by_css_selector("a")
for link in links:
    r = requests.head(link.get_attribute("href"))
    print(link.get_attribute("href"), r.status_code)
    assert r.status_code > 199 and r.status_code < 300

# Checking if the headers of the table are present
assert "ID" in driver.page_source
assert "Country" in driver.page_source
assert "Temperature 2016" in driver.page_source
assert "Rainfall 2016 (mm)" in driver.page_source
assert "Greenhouse Gas Emissions 2012" in driver.page_source

# Testing Search Bar
search_box = driver.find_element_by_xpath(
    "//*[@id='root']/div/div/div/div/div[1]/div[3]/div/input"
)
search_box.send_keys("United Kingdom")
driver.implicitly_wait(20)
assert "United Kingdom" in driver.page_source