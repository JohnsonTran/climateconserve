import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import {
  About,
  Climate,
  Countries,
  Home,
  NavBar,
  News,
  CountryPage,
  NewsPage,
  ClimatePage,
  SearchPage,
  Visualizations,
  ProviderVisualizations,
} from "./components";
import { Container } from "react-bootstrap";

function App() {
  return (
    <Router>
      <div>
        <NavBar />

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/country">
            <Container>
              <CountryPage />
            </Container>
          </Route>
          <Route path="/article">
            <Container>
              <NewsPage />
            </Container>
          </Route>
          <Route path="/climate">
            <Container>
              <ClimatePage />
            </Container>
          </Route>
          <Route path="/about">
            <Container>
              <About />
            </Container>
          </Route>
          <Route path="/countries">
            <Container>
              <Countries />
            </Container>
          </Route>
          <Route path="/climates">
            <Container>
              <Climate />
            </Container>
          </Route>
          <Route path="/news">
            <Container>
              <News />
            </Container>
          </Route>
          <Route path="/search">
            <Container>
              <SearchPage />
            </Container>
          </Route>
          <Route path="/visualizations">
            <Container>
              <Visualizations />
            </Container>
          </Route>
          <Route path="/provider-visualizations">
            <Container>
              <ProviderVisualizations />
            </Container>
          </Route>
          <Route path="/">
            <Container fluid>
              <Home />
            </Container>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
