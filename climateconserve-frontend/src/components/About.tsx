import React, { Component } from "react";
import axios from "axios";
import { Card, CardDeck } from "react-bootstrap";
import "./styles/About.css";
import VC from "../images/VC.jpeg";
import SS from "../images/SS.jpeg";
import JT from "../images/JT.jpg";
import KD from "../images/KD.jpg";
import JG from "../images/JG.jpg";

interface User {
  name: string;
  commits: number;
  closedIssues: number;
}

interface State {
  users: User[] | null;
  totalIssues: number;
  totalCommits: number;
}

export class About extends Component<{}, State> {
  state: State = {
    users: null,
    totalIssues: 0,
    totalCommits: 0,
  };

  getPaginatedResponse = async (
    apiRequest: string,
    page: number = 1
  ): Promise<[]> => {
    const query = `https://gitlab.com/api/v4/projects/24727917/${apiRequest}?per_page=100&page=${page}`;
    const response = await axios.get(query);
    const data = response.data;

    if (data.length > 0) {
      return data.concat(await this.getPaginatedResponse(apiRequest, page + 1));
    } else {
      return data;
    }
  };

  componentDidMount() {
    let johnson: User = { name: "Johnson Tran", commits: 0, closedIssues: 0 };
    let vivek: User = {
      name: "Vivek Chilakamarri",
      commits: 0,
      closedIssues: 0,
    };
    let sejal: User = { name: "Sejal Sharma", commits: 0, closedIssues: 0 };
    let jeffrey: User = { name: "Jeffrey Gordon", commits: 0, closedIssues: 0 };
    let kevin: User = { name: "Kevin Dao", commits: 0, closedIssues: 0 };
    let users = [johnson, vivek, sejal, jeffrey, kevin];

    // GET: all contributors to repo, including # commits, additions, deletions
    this.getPaginatedResponse("repository/commits", 1).then((res) => {
      const allCommits = res;
      const totalCommits = allCommits.length;

      let commit: any;
      for (commit of allCommits) {
        if (commit["author_name"] === "Johnson Tran") {
          johnson.commits++;
        }
        if (commit["author_name"] === "Vivek Chilakamarri") {
          vivek.commits++;
        }
        if (commit["author_name"] === "Sejal Sharma") {
          sejal.commits++;
        }
        if (commit["author_name"] === "Jeffrey Gordon") {
          jeffrey.commits++;
        }
        if (commit["author_name"] === "Kevin Dao") {
          kevin.commits++;
        }
      }
      this.setState({ users, totalCommits });
    });

    // GET: all issues from repo, count closed issues per contributor
    this.getPaginatedResponse("issues", 1).then((res) => {
      const totalIssues = res.length;

      // Count number of closed issues per team member
      let issue;
      for (issue of res) {
        if (issue["closed_by"]) {
          if (issue["closed_by"]["name"] === "Johnson Tran")
            johnson.closedIssues++;
          if (issue["closed_by"]["name"] === "Jeff G") jeffrey.closedIssues++;
          if (issue["closed_by"]["name"] === "Sejal Sharma")
            sejal.closedIssues++;
          if (issue["closed_by"]["name"] === "Kevin Dao") kevin.closedIssues++;
        }
      }

      this.setState({
        totalIssues,
      });
    });
  }

  render() {
    const { users, totalCommits, totalIssues } = this.state;
    if (!users && !totalCommits && totalIssues) {
      return <h1>Loading...</h1>;
    }

    return (
      <div>
        <h1>About</h1>
        <a href="https://youtu.be/xxKxC-byh2k">Presentation Video </a>
        <p>
          The site has data about countries, pollution emmisions of each
          country, and news about climate change. This website is intended to
          educate the general population about climate change and to showcase
          how countries have been fighting this problem.
        </p>
        <CardDeck>
          <Card style={{ width: "18rem" }}>
            <Card.Img variant="top" src={VC} />
            <Card.Body>
              <Card.Title>Vivek Chilakamarri</Card.Title>
              <Card.Text>
                Backend <br />
                My name is Vivek Chilakamarri and I am a junior studying
                computer science at UT Austin. I’m originally from Dallas, TX
                and like to play basketball, video games and read in my free
                time. Fun Fact: I’ve never lost a game of laser tag!
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">
                Commits: {this.state.users && this.state.users[1].commits}
              </small>
              <br />
              <small className="text-muted">
                Closed Issues:{" "}
                {this.state.users && this.state.users[1].closedIssues}
              </small>
            </Card.Footer>
          </Card>
          <Card style={{ width: "18rem" }}>
            <Card.Img variant="top" src={KD} />
            <Card.Body>
              <Card.Title>Kevin Dao</Card.Title>
              <Card.Text>
                Frontend <br />
                Hi, I'm a senior in Computer Science with a certificate in
                Design Strategies. When I have spare time, I like listening to
                music, doing photography, and listening to people speak foreign
                languages. Fun fact: I was born in Portland, Oregon!
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">
                Commits: {this.state.users && this.state.users[4].commits}
              </small>
              <br />
              <small className="text-muted">
                Closed Issues:{" "}
                {this.state.users && this.state.users[4].closedIssues}
              </small>
            </Card.Footer>
          </Card>
          <Card style={{ width: "18rem" }}>
            <Card.Img variant="top" src={JG} />
            <Card.Body>
              <Card.Title>Jeffrey Gordon</Card.Title>
              <Card.Text>
                Frontend <br />
                My name is Jeffrey Gordon. I am a junior studying Computer
                Science, with a minor in Business. I am from the Dallas-Forth
                Worth metroplex. In my free time, I like to listen to music,
                workout, and learn to cook new things. Fun Fact: I didn't know
                what a raspberry was until I was a senior in high school!
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">
                Commits: {this.state.users && this.state.users[3].commits}
              </small>
              <br />
              <small className="text-muted">
                Closed Issues:{" "}
                {this.state.users && this.state.users[3].closedIssues}
              </small>
            </Card.Footer>
          </Card>
        </CardDeck>
        <CardDeck>
          <Card className="h-25" style={{ width: "18rem" }}>
            <Card.Img variant="top" src={SS} />
            <Card.Body>
              <Card.Title>Sejal Sharma</Card.Title>
              <Card.Text>
                Backend <br />
                My name is Sejal Sharma. I am currently a junior majoring in
                Computer Science and minoring in Business. I have a strong focus
                for backend technology and I look forward to collaborating on
                this project with my team. Fun fact: I have a motorcycle
                license!
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">
                Commits: {this.state.users && this.state.users[2].commits}
              </small>
              <br />
              <small className="text-muted">
                Closed Issues:{" "}
                {this.state.users && this.state.users[2].closedIssues}
              </small>
            </Card.Footer>
          </Card>
          <Card style={{ width: "18rem" }}>
            <Card.Img variant="top" src={JT} />
            <Card.Body>
              <Card.Title>Johnson Tran</Card.Title>
              <Card.Text>
                Backend <br />
                I'm a junior studying Computer Science. In my free time, I enjoy
                browsing the internet and playing video games. Fun fact: I know
                how to solve a Rubik's Cube.
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">
                Commits: {this.state.users && this.state.users[0].commits}
              </small>
              <br />
              <small className="text-muted">
                Closed Issues:{" "}
                {this.state.users && this.state.users[0].closedIssues}
              </small>
            </Card.Footer>
          </Card>
        </CardDeck>
        <hr className="solid"></hr>
        <p>Total Commits: {totalCommits}</p>
        <p>Total Issues: {totalIssues}</p>
        <hr className="solid"></hr>
        <h3>Data Sources</h3>
        <ul>
          <li>
            <a href="https://datahelpdesk.worldbank.org/knowledgebase/articles/898599-indicator-api-queries">
              https://datahelpdesk.worldbank.org/knowledgebase/articles/898599-indicator-api-queries
            </a>{" "}
            - scraped total population, GDP
          </li>
          <li>
            <a href="https://www.thebasetrip.com/en/documentation/v3/basic">
              https://www.thebasetrip.com/en/documentation/v3/basics
            </a>{" "}
            - scraped country information and flag
          </li>
          <li>
            <a href="https://www.climatewatchdata.org/data-explorer/">
              https://www.climatewatchdata.org/data-explorer/
            </a>{" "}
            - scraped greenhouse gas emissions
          </li>
          <li>
            <a href="https://climateknowledgeportal.worldbank.org/download-data">
              https://climateknowledgeportal.worldbank.org/download-data
            </a>{" "}
            - scraped average rainfall and temperature
          </li>
          <li>
            <a href="https://data.worldbank.org/indicator/EN.ATM.PM25.MC.M3">
              https://data.worldbank.org/indicator/EN.ATM.PM25.MC.M3
            </a>{" "}
            - scraped PM2.5 levels
          </li>
          <li>
            <a href="https://stats.oecd.org/Index.aspx?DataSetCode=EXP_PM2_5">
              https://stats.oecd.org/Index.aspx?DataSetCode=EXP_PM2_5
            </a>{" "}
            - also scraped PM2.5 levels
          </li>
          <li>
            <a href="https://rapidapi.com/contextualwebsearch/api/web-search/">
              https://rapidapi.com/contextualwebsearch/api/web-search/
            </a>{" "}
            - scraped news articles
          </li>
        </ul>
        <h3>Tools</h3>
        <ul>
          <li>React - JavaScript framework used to create the frontend</li>
          <li>
            React-bootstrap - Module to create bootstrap components in React
          </li>
          <li>Flask - Python framework used to create the backend</li>
          <li>Postman - Platform that allowed us to create and test APIs</li>
          <li>AWS - Platform that hosts our website on the internet</li>
          <li>Gitlab - Source control tool for our repository</li>
          <li>Namecheap - Website that hosts our domain</li>
        </ul>
        <p>
          <a href="https://documenter.getpostman.com/view/14757536/TzCTa5SN">
            Our API
          </a>
        </p>
        <p>
          <a href="https://gitlab.com/JohnsonTran/climateconserve">
            Our Gitlab Repo
          </a>
        </p>
      </div>
    );
  }
}
