import React, { Component } from "react";
import axios from "axios";
import { numberWithCommas } from "../utils";
import MaterialTable from "material-table";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { CountryObj } from "./types";
import "./styles/CountryPage.css";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export class Countries extends Component<
  {},
  { countries: CountryObj[] | undefined
    temp: CountryObj[] | undefined 
    checkNum: number }
> {
  constructor(props = {}) {
    super(props);
    this.state = {
      countries: undefined,
      temp: undefined,
      checkNum: 0
    };
  }

  componentDidMount() {
    // GET all country data from API
    axios.get("https://www.climateconserve.me/api/countries").then((res) => {
      const data = res.data;
      this.setState({ countries: data });
      this.setState({ temp: data });
    });
  }

  // const [data, setData] = useState<CountryObj[]>(this.state.countries);
  // const [checked, setChecked] = useState(false);

  filterValue(checked:boolean, value:number) {
    if (checked) {
      if (value == 1) {
        const filtered = this.state.countries.filter(d => d.name.charCodeAt(0) >= "A".charCodeAt(0) &&
        d.name.charCodeAt(0) <= "J".charCodeAt(0));
        this.setState({ temp: filtered });
      } 
      else if (value == 2) {
        const filtered = this.state.countries.filter(d => d.name.charCodeAt(0) >= "K".charCodeAt(0) &&
        d.name.charCodeAt(0) <= "R".charCodeAt(0));
        this.setState({ temp: filtered });
      }
      else if (value == 3) {
        const filtered = this.state.countries.filter(d => d.name.charCodeAt(0) >= "S".charCodeAt(0) &&
        d.name.charCodeAt(0) <= "Z".charCodeAt(0));
        this.setState({ temp: filtered });
      }
      else if (value == 4) {
        const filtered = this.state.countries.filter(d => d.capital.charCodeAt(0) >= "A".charCodeAt(0) &&
        d.capital.charCodeAt(0) <= "J".charCodeAt(0));
        this.setState({ temp: filtered });
      } 
      else if (value == 5) {
        const filtered = this.state.countries.filter(d => d.capital.charCodeAt(0) >= "K".charCodeAt(0) &&
        d.capital.charCodeAt(0) <= "R".charCodeAt(0));
        this.setState({ temp: filtered });
      }
      else if (value == 6) {
        const filtered = this.state.countries.filter(d => d.capital.charCodeAt(0) >= "S".charCodeAt(0) &&
        d.capital.charCodeAt(0) <= "Z".charCodeAt(0));
        this.setState({ temp: filtered });
      }
      else if (value == 7) {
        const filtered = this.state.countries.filter(d => d.region == "North America");
        this.setState({ temp: filtered });
      }
      else if (value == 8) {
        const filtered = this.state.countries.filter(d => d.region == "Latin America \u0026 Caribbean");
        this.setState({ temp: filtered });
      }
      else if (value == 9) {
        const filtered = this.state.countries.filter(d => d.region == "Europe \u0026 Central Asia");
        this.setState({ temp: filtered });
      }
      else if (value == 10) {
        const filtered = this.state.countries.filter(d => d.region == "East Asia \u0026 Pacific");
        this.setState({ temp: filtered });
      }
      else if (value == 11) {
        const filtered = this.state.countries.filter(d => d.region == "South Asia");
        this.setState({ temp: filtered });
      }
      else if (value == 12) {
        const filtered = this.state.countries.filter(d => d.region == "Middle East \u0026 North Africa");
        this.setState({ temp: filtered });
      }
      else if (value == 13) {
        const filtered = this.state.countries.filter(d => d.region == "Sub-Saharan Africa");
        this.setState({ temp: filtered });
      }
      else if (value == 14) {
        const filtered = this.state.countries.filter(d => d.population <= 10000000);
        this.setState({ temp: filtered });
      }
      else if (value == 15) {
        const filtered = this.state.countries.filter(d => d.population > 10000000 && d.population <= 100000000);
        this.setState({ temp: filtered });
      }
      else if (value == 16) {
        const filtered = this.state.countries.filter(d => d.population > 100000000);
        this.setState({ temp: filtered });
      }
      else if (value == 17) {
        const filtered = this.state.countries.filter(d => d.income_level == "Low income");
        this.setState({ temp: filtered });
      }
      else if (value == 18) {
        const filtered = this.state.countries.filter(d => d.income_level == "Lower middle income");
        this.setState({ temp: filtered });
      }
      else if (value == 19) {
        const filtered = this.state.countries.filter(d => d.income_level == "Upper middle income");
        this.setState({ temp: filtered });
      }
      else if (value == 20) {
        const filtered = this.state.countries.filter(d => d.income_level == "High income");
        this.setState({ temp: filtered });
      }
      else {
        this.setState({ temp: this.state.countries });
      }
      this.setState({ checkNum: value });
    }
    else {
      this.setState({ checkNum: 0 });
      this.setState({ temp: this.state.countries });
    }
  };

  render() {
    if (!this.state.countries) {
      return <h1>Loading...</h1>;
    }

    return (
      <div>
        <Container>
          <Row>
            <Col>
              <div>
                <h5>Country name:</h5>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkNum == 1}
                      color="primary"
                      onChange={e => this.filterValue(e.target.checked, 1)}
                    />
                  }
                  label="A-J"
                  labelPlacement="end"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkNum == 2}
                      color="primary"
                      onChange={e => this.filterValue(e.target.checked, 2)}
                    />
                  }
                  label="K-R"
                  labelPlacement="end"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkNum == 3}
                      color="primary"
                      onChange={e => this.filterValue(e.target.checked, 3)}
                    />
                  }
                  label="S-Z"
                  labelPlacement="end"
                />
              </div>
            </Col>
            <Col>
              <h5>Capital name:</h5>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 4}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 4)}
                      />
                    }
                    label="A-J"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 5}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 5)}
                      />
                    }
                    label="K-R"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 6}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 6)}
                      />
                    }
                    label="S-Z"
                    labelPlacement="end"
                  />
                </div>
            </Col>
          </Row>
          <Row>
          <Col>
              <h5>Region:</h5>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 7}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 7)}
                      />
                    }
                    label="North America"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 8}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 8)}
                      />
                    }
                    label="Latin America & Caribbean"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 9}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 9)}
                      />
                    }
                    label="Europe & Central Asia"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 10}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 10)}
                      />
                    }
                    label="East Asia & Pacific"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 11}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 11)}
                      />
                    }
                    label="South Asia"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 12}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 12)}
                      />
                    }
                    label="Middle East & North Africa"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 13}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 13)}
                      />
                    }
                    label="Sub-Saharan Africa"
                    labelPlacement="end"
                  />
                </div>
            </Col>
            <Col>
              <h5>Population:</h5>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 14}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 14)}
                      />
                    }
                    label="Low Population"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 15}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 15)}
                      />
                    }
                    label="Medium Population"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 16}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 16)}
                      />
                    }
                    label="High Population"
                    labelPlacement="end"
                  />
                </div>
                <h5>Income Level:</h5>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 17}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 17)}
                      />
                    }
                    label="Low Income"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 18}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 18)}
                      />
                    }
                    label="Lower middle income"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 19}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 19)}
                      />
                    }
                    label="Upper middle income"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 20}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 20)}
                      />
                    }
                    label="High income"
                    labelPlacement="end"
                  />
                </div>
            </Col>
          </Row>
        </Container>
        <MaterialTable
          columns={[
            {
              title: "ID",
              field: "id",
              type: "numeric",
              align: "left",
              cellStyle: { width: "1%" },
              filtering: false
            },
            {
              title: "Country",
              field: "name",
              render: (rowData) => (
                <a href={`country?id=${rowData.id}`}>{rowData.name}</a>
              ),
            },
            {
              title: "Capital",
              field: "capital",
            },
            { title: "Region", 
              field: "region",
            },
            { title: "Population Size", 
              field: "population", 
              type: "numeric",
            },
            { title: "Income Level", 
              field: "income_level",
            },
          ]}
          data={this.state.temp ? this.state.temp : []}
          title="Countries"
          options={{
            headerStyle: {
              backgroundColor: "#B3E5FC",
              color: "#FFF",
            },
            pageSize: 10,
          }}
        />
      </div>
    );
  }
}
