import React, { Component } from "react";
import { CountryObj } from "./types";
import axios from "axios";
import * as d3 from "d3";
import {
  PieChart,
  Pie,
  ScatterChart,
  Scatter,
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import orgs from "../data/orgs.json";
import city from "../data/city.json";
import school from "../data/school.json";


export class ProviderVisualizations extends Component<
  {},
  { countries: CountryObj[] | undefined }
> {
  constructor(props = {}) {
    super(props);
    this.state = { countries: undefined };
  }

  componentDidMount() {
    // GET all country data from API
    axios.get("https://www.climateconserve.me/api/countries").then((res) => {
      const data = res.data;
      this.setState({ countries: data });
    });
  }

  render() {
    return (
      <div>
        <h1>Provider Visualizations</h1>
        <h3>Amount of Charitable Organizations by State</h3>
        <PieChart width={550} height={550}>
        <Pie
            dataKey="value"
            isAnimationActive={true}
            data={orgs}
            cx={300}
            cy={300}
            outerRadius={200}
            fill="#fff"
            label
        />
        <Tooltip />
        </PieChart>

        <h3>Median Income vs Poverty Population Percent</h3>
        <ScatterChart
          width={400}
          height={400}
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid />
          <XAxis type="number" dataKey="income" name="income" unit=" dollars" />
          <YAxis type="number" dataKey="vacantHousing" name="vacant housing percent" unit="%" />
          <Tooltip cursor={{ strokeDasharray: '3 3' }} />
          <Scatter name="City" data={city} fill="#F47A1F" />
        </ScatterChart>

        <h3>Average School Rank Percentile per State</h3>
        <BarChart
            width={700}
            height={400}
            data={school}
            margin={{
              top: 5,
              right: 30,
              left: 50,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis
              dataKey="state"
              angle={80}
              textAnchor="start"
              interval={0}
              height={100}
            />
            <YAxis />
            <Tooltip
              formatter={(value: number) =>
                new Intl.NumberFormat("en").format(value)
              }
            />
            <Bar dataKey="avg" fill="#007CC3" />
          </BarChart>
      </div>
    );
  }
}
