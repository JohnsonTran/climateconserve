import React, { Component } from "react";
import Image from "react-bootstrap/Image";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { CountryObj } from "./types";
import { numberWithCommas } from "../utils";
import GoogleMapReact from "google-map-react";
import axios from "axios";
import { LatLngExpression } from "leaflet";

export class CountryPage extends Component<
  {},
  { currentCountry: CountryObj | undefined; 
    googleApiKey: string
    articleID: number
   }
> {
  constructor(props = {}) {
    super(props);
    this.state = {
      currentCountry: undefined,
      googleApiKey: "AIzaSyAvN3GNIb8EyzytP69FWicA1Xcje4Ss-zY",
      articleID: 1
    };
  }

  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    const countryId = urlParams.get("id");

    // GET all country data from API
    axios
      .get("https://www.climateconserve.me/api/country/" + countryId)
      .then((res) => {
        this.setState({ currentCountry: res.data });
      });
    //find article id
    axios
      .get("https://www.climateconserve.me/api/news/" + countryId)
      .then((res) => {
        console.log(res.data)
        //get a random article
        let size = res.data.length;
        let randomArticle = res.data[Math.floor(Math.random() * size)];
        this.setState({ articleID: randomArticle["article-id"] });
      });
  }

  render() {
    const country = this.state.currentCountry;
    if (
      !country ||
      country == undefined ||
      country.latitude == undefined ||
      country.longitude == undefined
    ) {
      return <h1>Loading...</h1>;
    }

    const position: LatLngExpression = [
      this.state.currentCountry!.latitude,
      this.state.currentCountry!.longitude,
    ];

    return (
      <div style={{ textAlign: "center" }}>
        <h1>{country.name}</h1>
        <Image
          src={country.flag}
          alt={"Flag Loading..."}
          style={{ maxWidth: "600px" }}
        />
        <hr></hr>
        <Row>
          <Col>
            <p>Region: {country.region}</p>
          </Col>
          <Col>
            {" "}
            <p>Capital: {country.capital}</p>
          </Col>
          <Col>
            <p>Gross Domestic Product (USD): {numberWithCommas(country.gdp)}</p>
          </Col>
        </Row>
        <Row>
          <Col>
            {" "}
            <p>Income level: {country.income_level}</p>
          </Col>
          <Col>
            {" "}
            <p>Population: {numberWithCommas(country.population)} </p>
          </Col>
          <Col>
            {" "}
            <p>World Bank GINI Index (Income Inequality): {country.gini}</p>
          </Col>
        </Row>
        <Row>
          <Col>
            {" "}
            <p>Land Size (Area): {numberWithCommas(country.area)} mi&sup2;</p>
          </Col>
          <Col>
            {" "}
            <p>Latitude: {country.latitude}</p>
          </Col>
          <Col>
            {" "}
            <p>Longitude: {country.longitude}</p>
          </Col>
        </Row>

        {/* TODO: fix links */}
        <p>
          <a href={`/climate?id=${country.id}`}>Climate in {country.name}</a>
        </p>
        <p>
          <a href={`/article?id=${this.state.articleID}`}>News in {country.name}</a>
        </p>
        <hr></hr>
        <div style={{ height: "100vh", width: "100%" }}>
          <GoogleMapReact
            bootstrapURLKeys={{ key: this.state.googleApiKey }}
            defaultCenter={{ lat: position[0], lng: position[1] }}
            defaultZoom={5}
          ></GoogleMapReact>
        </div>
      </div>
    );
  }
}
