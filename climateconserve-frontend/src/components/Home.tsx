import React, { Component } from "react";
import { Row, Col, Jumbotron, Container, Card } from "react-bootstrap";
import country from "../images/country.png";
import climate from "../images/climate.png";
import news from "../images/news.png";
import "./styles/Home.css";

export class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <Jumbotron className="bg-light text-dark" fluid>
          <Container>
            <h1>Our Planet</h1>
            <h3>
              The first step to solving climate change is raising awareness.
            </h3>
            <a href="https://youtu.be/xxKxC-byh2k">Presentation Video </a>
          </Container>
        </Jumbotron>
        <Container>
          <Row>
            <Col>
              <Card className="text-center" style={{ width: "18rem" }}>
                <Card.Img
                  variant="top"
                  src={country}
                  style={{ height: "15vw" }}
                />
                <Card.Body>
                  <Card.Title>Country</Card.Title>
                  <Card.Text>
                    Learn about countries all over the world!
                  </Card.Text>
                  <Card.Link href="/countries">Learn more</Card.Link>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card className="text-center" style={{ width: "18rem" }}>
                <Card.Img
                  variant="top"
                  src={climate}
                  style={{ height: "15vw" }}
                />
                <Card.Body>
                  <Card.Title>Climate</Card.Title>
                  <Card.Text>
                    Learn about a country's recent climate conditions!
                  </Card.Text>
                  <Card.Link href="/climates">Learn more</Card.Link>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card className="text-center" style={{ width: "18rem" }}>
                <Card.Img variant="top" src={news} style={{ height: "15vw" }} />
                <Card.Body>
                  <Card.Title>News</Card.Title>
                  <Card.Text>
                    Learn about major events happening globally!
                  </Card.Text>
                  <Card.Link href="/news">Learn more</Card.Link>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}
