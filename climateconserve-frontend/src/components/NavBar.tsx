import React, { Component } from "react";
import { Nav, Navbar, Form, FormControl, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export class NavBar extends Component<{}, { currSearch: string }> {
  constructor(props = {}) {
    super(props);
    this.state = {
      currSearch: "",
    };
  }
  render() {
    return (
      <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand>Climate Conserve</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={Link} to="/about">
              About
            </Nav.Link>
            <Nav.Link as={Link} to="/countries">
              Countries
            </Nav.Link>
            <Nav.Link as={Link} to="/climates">
              Climates
            </Nav.Link>
            <Nav.Link as={Link} to="/news">
              News
            </Nav.Link>
            <Nav.Link as={Link} to="/visualizations">
              Our Visualizations
            </Nav.Link>
            <Nav.Link as={Link} to="/provider-visualizations">
              Provider Visualizations
            </Nav.Link>
          </Nav>
          <Form inline>
            <FormControl
              value={this.state.currSearch}
              onChange={(e) => this.setState({ currSearch: e.target.value })}
              type="text"
              placeholder="Look for anything!"
            />
            <Button
              variant="primary"
              href={`/search?query=${this.state.currSearch}`}
            >
              Search
            </Button>{" "}
          </Form>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
