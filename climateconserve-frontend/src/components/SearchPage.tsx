import React, { Component } from "react";
// import { Row, Col, Image } from "react-bootstrap";
import { CountryObj, ClimateObj, NewsObj } from "./types";
import axios from "axios";
import { Row, Col, Card, Button, Form } from "react-bootstrap";
import { formatString } from "../utils";
import Highlighter from "react-highlight-words";
import ReactPaginate from "react-paginate";
import { Climate } from "./Climate";

export class SearchPage extends Component<
  {},
  {
    countries: CountryObj[] | undefined;
    climates: ClimateObj[] | undefined;
    articles: NewsObj[] | undefined;
    currSearch: string;
    currentArticlePage: number;
    totalArticlePages: number;
    totalArticles: number;
    currentCountryPage: number;
    totalCountryPages: number;
    totalCountries: number;
    currentClimatePage: number;
    totalClimatePages: number;
    totalClimates: number;
    activeSearch: any;
    //TODO: add total instances
  }
> {
  constructor(props = {}) {
    super(props);
    this.state = {
      countries: undefined,
      climates: undefined,
      articles: undefined,
      currSearch: "",
      currentArticlePage: 1,
      totalArticlePages: 0,
      totalArticles: 0,
      currentCountryPage: 1,
      totalCountryPages: 0,
      totalCountries: 0,
      currentClimatePage: 1,
      totalClimatePages: 0,
      totalClimates: 0,
      activeSearch: [],
    };
  }
  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    let query = urlParams.get("query");
    query = query == null ? "" : query;
    this.setState({ currSearch: query });
    this.search(query);
  }

  search = (query: string) => {
    this.searchCountry(query);
    this.searchNews(query);
    this.searchClimate(query);
  };

  searchClimate = (query: string) => {
    axios
      .get(`https://www.climateconserve.me/api/search_climate/${query}/1`)
      .then((res) => {
        const data = res.data;
        const totalClimatePages = data[0]["total-pages"]; // store total pages in News component state
        const totalClimates = data[0]["total-climates"];
        delete data[0]; // delete curr-page and total-page entry
        console.log(res);
        this.setState({
          climates: data,
          totalClimatePages,
          totalClimates,
          currentClimatePage: 1,
          activeSearch: [this.state.currSearch],
        });
        this.buildClimateCards();
        window.scroll({ top: 0, left: 0, behavior: "smooth" });
      })
      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
      });
  };

  searchNews = (query: string) => {
    axios
      .get(`https://www.climateconserve.me/api/search_news/${query}/1`)
      .then((res) => {
        const data = res.data;
        const totalArticlePages = data[0]["total-pages"]; // store total pages in News component state
        const totalArticles = data[0]["total-articles"];
        delete data[0]; // delete curr-page and total-page entry
        this.setState({
          articles: data,
          totalArticlePages,
          totalArticles,
          currentArticlePage: 1,
          activeSearch: [this.state.currSearch],
        });
        this.buildNewsCards();
        window.scroll({ top: 0, left: 0, behavior: "smooth" });
      })
      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
      });
  };

  searchCountry = (query: string) => {
    axios
      .get(`https://www.climateconserve.me/api/search_country/${query}/1`)
      .then((res) => {
        const data = res.data;
        const totalCountryPages = data[0]["total-pages"]; // store total pages in News component state
        const totalCountries = data[0]["total-countries"];
        delete data[0]; // delete curr-page and total-page entry
        this.setState({
          countries: data,
          totalCountryPages,
          totalCountries,
          currentCountryPage: 1,
          activeSearch: [this.state.currSearch],
        });
        this.buildCountryCards();
        window.scroll({ top: 0, left: 0, behavior: "smooth" });
      })
      .catch(function (error) {
        if (error.response) {
          // Request made and server responded
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
      });
  };

  //build country cards
  buildClimateCards() {
    return (
      <Row>
        {this.state.climates &&
          this.state.climates.map((climate: ClimateObj) => {
            return (
              <Col lg={4}>
                <Card>
                  <Card.Body>
                    <Card.Title>
                      <Highlighter
                        highlightClassName="highlighted"
                        searchWords={this.state.activeSearch}
                        autoEscape={true}
                        textToHighlight={climate.name}
                      />
                    </Card.Title>
                    <Card.Text>
                      Climate Zone:{" "}
                      <Highlighter
                        highlightClassName="highlighted"
                        searchWords={this.state.activeSearch}
                        autoEscape={true}
                        textToHighlight={climate.climate_zone}
                      />
                    </Card.Text>
                    <Button
                      variant="primary"
                      href={`/climate?id=${climate["id"]}`}
                    >
                      Read More
                    </Button>
                  </Card.Body>
                  <Card.Footer>
                    <small className="text-muted">
                      {" "}
                      2012 GHG Emissions:{" "}
                      <i>
                        <Highlighter
                          highlightClassName="highlighted"
                          searchWords={this.state.activeSearch}
                          autoEscape={true}
                          textToHighlight={climate.ghg["2012"]}
                        />
                      </i>
                    </small>
                    <br />
                    <small className="text-muted">
                      {" "}
                      2016 Rainfall: <i>{climate.rainfall["2016"]}</i>
                    </small>
                    <br />
                    <small className="text-muted">
                      Temperature:{" "}
                      <i>
                        <Highlighter
                          highlightClassName="highlighted"
                          searchWords={this.state.activeSearch}
                          autoEscape={true}
                          textToHighlight={climate.temperature["2016"]}
                        />
                      </i>
                    </small>
                    <br />
                    <small className="text-muted">
                      Climate ID: <i>{climate["id"]}</i>
                    </small>
                  </Card.Footer>
                </Card>
              </Col>
            );
          })}
      </Row>
    );
  }

  //build country cards
  buildCountryCards() {
    return (
      <Row>
        {this.state.countries &&
          this.state.countries.map((country: CountryObj) => {
            return (
              <Col lg={4}>
                <Card>
                  <Card.Img variant="top" src={country.flag} />
                  <Card.Body>
                    <Card.Title>
                      <Highlighter
                        highlightClassName="highlighted"
                        searchWords={this.state.activeSearch}
                        autoEscape={true}
                        textToHighlight={country.name}
                      />
                    </Card.Title>
                    <Card.Text>
                      Capital:{" "}
                      <Highlighter
                        highlightClassName="highlighted"
                        searchWords={this.state.activeSearch}
                        autoEscape={true}
                        textToHighlight={country.capital}
                      />
                    </Card.Text>
                    <Button
                      variant="primary"
                      href={`/country?id=${country["id"]}`}
                    >
                      Read More
                    </Button>
                  </Card.Body>
                  <Card.Footer>
                    <small className="text-muted">
                      {" "}
                      Income Level:{" "}
                      <i>
                        <Highlighter
                          highlightClassName="highlighted"
                          searchWords={this.state.activeSearch}
                          autoEscape={true}
                          textToHighlight={country.income_level}
                        />
                      </i>
                    </small>
                    <br />
                    <small className="text-muted">
                      {" "}
                      Population: <i>{country.population}</i>
                    </small>
                    <br />
                    <small className="text-muted">
                      Region:{" "}
                      <i>
                        <Highlighter
                          highlightClassName="highlighted"
                          searchWords={this.state.activeSearch}
                          autoEscape={true}
                          textToHighlight={country.region}
                        />
                      </i>
                    </small>
                    <br />
                    <small className="text-muted">
                      Country ID: <i>{country["id"]}</i>
                    </small>
                  </Card.Footer>
                </Card>
              </Col>
            );
          })}
      </Row>
    );
  }

  //build news cards
  buildNewsCards() {
    return (
      <Row>
        {this.state.articles &&
          this.state.articles.map((article: NewsObj) => {
            return (
              <Col lg={4}>
                <Card>
                  <Card.Img variant="top" src={article.thumbnail} />
                  <Card.Body>
                    <Card.Title>
                      <Highlighter
                        highlightClassName="highlighted"
                        searchWords={this.state.activeSearch}
                        autoEscape={true}
                        textToHighlight={article.title}
                      />
                    </Card.Title>
                    <Card.Text>
                      <Highlighter
                        highlightClassName="highlighted"
                        searchWords={this.state.activeSearch}
                        autoEscape={true}
                        textToHighlight={article.description}
                      />
                    </Card.Text>
                    <Button
                      variant="primary"
                      href={`/article?id=${article["article-id"]}`}
                    >
                      Read More
                    </Button>
                  </Card.Body>
                  <Card.Footer>
                    <small className="text-muted">
                      {" "}
                      Country:{" "}
                      <i>
                        <Highlighter
                          highlightClassName="highlighted"
                          searchWords={this.state.activeSearch}
                          autoEscape={true}
                          textToHighlight={article.country}
                        />
                      </i>
                    </small>
                    <br />
                    <small className="text-muted">
                      {" "}
                      Date Posted:{" "}
                      <i>{new Date(article.date).toDateString()}</i>
                    </small>
                    <br />
                    <small className="text-muted">
                      Provider:{" "}
                      <i>
                        <Highlighter
                          highlightClassName="highlighted"
                          searchWords={this.state.activeSearch}
                          autoEscape={true}
                          textToHighlight={article.provider}
                        />
                      </i>
                    </small>
                    <br />
                    <small className="text-muted">
                      Article ID: <i>{article["article-id"]}</i>
                    </small>
                  </Card.Footer>
                </Card>
              </Col>
            );
          })}
      </Row>
    );
  }

  handleCountryPageClick = (e: any) => {
    const selectedPage = e.selected + 1; // +1 because react-paginate starts on page 0
    this.setState({ currentCountryPage: selectedPage }, () => {
      this.buildCountryCards();
      window.scroll({ top: 0, left: 0, behavior: "smooth" });
    });
    axios
      .get(
        `https://www.climateconserve.me/api/search_country/${
          this.state.currSearch
        }/${this.state.currentCountryPage + 1}`
      )
      .then((res) => {
        const data = res.data;
        delete data[0];
        this.setState({
          countries: data,
        });
      });
  };

  handleClimatePageClick = (e: any) => {
    const selectedPage = e.selected + 1; // +1 because react-paginate starts on page 0
    this.setState({ currentClimatePage: selectedPage }, () => {
      this.buildClimateCards();
      window.scroll({ top: 0, left: 0, behavior: "smooth" });
    });
    axios
      .get(
        `https://www.climateconserve.me/api/search_climate/${
          this.state.currSearch
        }/${this.state.currentClimatePage + 1}`
      )
      .then((res) => {
        const data = res.data;
        delete data[0];
        this.setState({
          climates: data,
        });
      });
  };

  handleNewsPageClick = (e: any) => {
    const selectedPage = e.selected + 1; // +1 because react-paginate starts on page 0
    this.setState({ currentArticlePage: selectedPage }, () => {
      this.buildNewsCards();
      window.scroll({ top: 0, left: 0, behavior: "smooth" });
    });
    axios
      .get(
        `https://www.climateconserve.me/api/search_news/${
          this.state.currSearch
        }/${this.state.currentArticlePage + 1}`
      )
      .then((res) => {
        const data = res.data;
        delete data[0];
        this.setState({
          articles: data,
        });
      });
  };

  render() {
    if (this.state.currSearch == "") {
      return <h1>Enter a query to search ClimateConserve!</h1>;
    }
    return (
      <div>
        <div>
          <h1>Showing Country Results For: {this.state.currSearch}</h1>
          {this.buildCountryCards()}
          <ReactPaginate
            previousLabel={"← Previous"}
            nextLabel={"Next →"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.state.totalCountryPages}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={this.handleCountryPageClick}
            containerClassName={"pagination"}
            activeClassName={"pagination-active"}
            disabledClassName={"pagination-disabled"}
            nextLinkClassName={"pagination__link"}
            previousLinkClassName={"pagination__link"}
          />
        </div>
        <div>
          <h1>Showing News Results For: {this.state.currSearch}</h1>
          {this.buildNewsCards()}
          <ReactPaginate
            previousLabel={"← Previous"}
            nextLabel={"Next →"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.state.totalArticlePages}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={this.handleNewsPageClick}
            containerClassName={"pagination"}
            activeClassName={"pagination-active"}
            disabledClassName={"pagination-disabled"}
            nextLinkClassName={"pagination__link"}
            previousLinkClassName={"pagination__link"}
          />
        </div>
        <div>
          <h1>Showing Climate Results For: {this.state.currSearch}</h1>
          {this.buildClimateCards()}
          <ReactPaginate
            previousLabel={"← Previous"}
            nextLabel={"Next →"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={this.state.totalClimatePages}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={this.handleClimatePageClick}
            containerClassName={"pagination"}
            activeClassName={"pagination-active"}
            disabledClassName={"pagination-disabled"}
            nextLinkClassName={"pagination__link"}
            previousLinkClassName={"pagination__link"}
          />
        </div>
      </div>
    );
  }
}
