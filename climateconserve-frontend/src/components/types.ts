export interface CountryObj {
  area: number;
  capital: string;
  flag: string;
  gdp: string;
  gini: number;
  id: number;
  income_level: string;
  latitude: number;
  longitude: number;
  name: string;
  population: number;
  region: string;
}

export interface ClimateObj {
  aqi: number;
  ch4: { [key: string]: string };
  climate_zone: string;
  co2: { [key: string]: string };
  ghg: { [key: string]: string };
  id: number;
  name: string;
  no2: { [key: string]: string };
  pm10: number;
  pm25: { [key: string]: string };
  rainfall: { [key: string]: string };
  temperature: { [key: string]: string };
}

export interface NewsObj {
  "article-id": number;
  content: string;
  country: string;
  country_id: number;
  date: Date;
  description: string;
  language: string;
  provider: string;
  thumbnail: string;
  title: string;
  url: string;
  word_count: number;
}

export interface ClimateData {
  x: number[];
  y: number[];
}
