import React, { Component, MouseEventHandler } from "react";
import axios from "axios";
import { Row, Col, Card, Button, Form } from "react-bootstrap";
import { NewsObj } from "./types";
import ReactPaginate from "react-paginate";
import Highlighter from "react-highlight-words";
import "./styles/News.css";
import "./styles/Highlight.css";

export class News extends Component<
  {},
  {
    articles: NewsObj[] | undefined;
    currentPage: number;
    totalPages: number;
    totalArticles: number;
    currSearch: string;
    activeSearch: string[];
  }
> {
  constructor(props = {}) {
    super(props);
    this.state = {
      articles: undefined,
      currentPage: 1,
      totalPages: 0,
      totalArticles: 0,
      currSearch: "", // stores query in search bar
      activeSearch: [], // insert currSearch on search click, for highlighting
    };
  }

  componentDidMount() {
    // GET all climate data from API
    axios.get("https://www.climateconserve.me/api/all-news/1").then((res) => {
      const data = res.data;
      const totalPages = data[0]["total-pages"]; // store total pages in News component state
      const totalArticles = data[0]["total-articles"];
      delete data[0]; // delete curr-page and total-page entry
      this.setState({ articles: data, totalPages, totalArticles });
    });
  }

  receivedData() {
    // No search query, display all news articles paginated
    if (this.state.currSearch === "") {
      axios
        .get(
          `https://www.climateconserve.me/api/all-news/${this.state.currentPage}`
        )
        .then((res) => {
          const data = res.data;
          delete data[0];
          this.setState({
            articles: data,
          });
        });
    } else {
      axios
        .get(
          `https://www.climateconserve.me/api/search_news/${this.state.currSearch}/${this.state.currentPage}`
        )
        .then((res) => {
          const data = res.data;
          delete data[0];
          this.setState({
            articles: data,
          });
        });
    }
  }

  handlePageClick = (e: any) => {
    const selectedPage = e.selected + 1; // +1 because react-paginate starts on page 0
    this.setState({ currentPage: selectedPage }, () => {
      this.receivedData();
      this.buildCards();
      window.scroll({ top: 0, left: 0, behavior: "smooth" });
    });
  };

  // Query first page of searched news articles
  search = () => {
    // Empty search query, get all news
    if (this.state.currSearch === "") {
      this.receivedData();
      this.setState({ activeSearch: [] });
    } else {
      axios
        .get(
          `https://www.climateconserve.me/api/search_news/${this.state.currSearch}/1`
        )
        .then((res) => {
          const data = res.data;
          if (data.length == 0) {
            this.setState({
              articles: data,
              totalPages: 0,
              totalArticles: 0,
              currentPage: 1,
              activeSearch: [this.state.currSearch],
            });
          } else {
            const totalPages = data[0]["total-pages"]; // store total pages in News component state
            const totalArticles = data[0]["total-articles"];
            delete data[0]; // delete curr-page and total-page entry
            this.setState({
              articles: data,
              totalPages,
              totalArticles,
              currentPage: 1,
              activeSearch: [this.state.currSearch],
            });
          }
          this.buildCards();
          window.scroll({ top: 0, left: 0, behavior: "smooth" });
        });
    }
  };

  buildCards() {
    return (
      <Row>
        {this.state.articles &&
          this.state.articles.map((article: NewsObj) => {
            return (
              <Col lg={4}>
                <Card>
                  <Card.Img variant="top" src={article.thumbnail} />
                  <Card.Body>
                    <Card.Title>
                      <Highlighter
                        highlightClassName="highlighted"
                        searchWords={this.state.activeSearch}
                        autoEscape={true}
                        textToHighlight={article.title}
                      />
                    </Card.Title>
                    <Card.Text>
                      <Highlighter
                        highlightClassName="highlighted"
                        searchWords={this.state.activeSearch}
                        autoEscape={true}
                        textToHighlight={article.description}
                      />
                    </Card.Text>
                    <Button
                      variant="primary"
                      href={`/article?id=${article["article-id"]}`}
                    >
                      Read More
                    </Button>
                  </Card.Body>
                  <Card.Footer>
                    <small className="text-muted">
                      {" "}
                      Country:{" "}
                      <i>
                        <Highlighter
                          highlightClassName="highlighted"
                          searchWords={this.state.activeSearch}
                          autoEscape={true}
                          textToHighlight={article.country}
                        />
                      </i>
                    </small>
                    <br />
                    <small className="text-muted">
                      {" "}
                      Date Posted:{" "}
                      <i>{new Date(article.date).toDateString()}</i>
                    </small>
                    <br />
                    <small className="text-muted">
                      Provider:{" "}
                      <i>
                        <Highlighter
                          highlightClassName="highlighted"
                          searchWords={this.state.activeSearch}
                          autoEscape={true}
                          textToHighlight={article.provider}
                        />
                      </i>
                    </small>
                    <br />
                    <small className="text-muted">
                      Article ID: <i>{article["article-id"]}</i>
                    </small>
                  </Card.Footer>
                </Card>
              </Col>
            );
          })}
      </Row>
    );
  }

  render() {
    if (!this.state.articles) {
      return <h1>Loading...</h1>;
    }

    return (
      <div>
        <h1>News</h1>
        <Form>
          <Form.Row>
            <Col>
              <Form.Control
                value={this.state.currSearch}
                onChange={(e) => this.setState({ currSearch: e.target.value })}
                id="searchForm"
                type="text"
                placeholder="Search for news"
              />
            </Col>
            <Col>
              <Button variant="primary" onClick={this.search}>
                Search
              </Button>
            </Col>
          </Form.Row>
        </Form>
        <br></br>
        {this.buildCards()}
        <p>Total Articles: {this.state.totalArticles}</p>
        <ReactPaginate
          previousLabel={"← Previous"}
          nextLabel={"Next →"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={this.state.totalPages}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={this.handlePageClick}
          containerClassName={"pagination"}
          activeClassName={"pagination-active"}
          disabledClassName={"pagination-disabled"}
          nextLinkClassName={"pagination__link"}
          previousLinkClassName={"pagination__link"}
        />
      </div>
    );
  }
}
