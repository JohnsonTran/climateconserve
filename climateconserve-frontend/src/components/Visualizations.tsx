import React, { Component } from "react";
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Pie,
  PieChart,
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis
} from "recharts";
import { CountryObj, ClimateObj } from "./types";
import population from "../data/population.json";
import emissions from "../data/emissions.json";
import news from "../data/newsres.json";


export class Visualizations extends Component<
  {},
  { countries: CountryObj[] | undefined, climates: ClimateObj[] | undefined }
> {
  constructor(props = {}) {
    super(props);
    this.state = { countries: undefined, climates: undefined };
  }

  render() {
    return (
      <div style={{ height: "500px" }}>
        <h1>Our Visualizations</h1>
        <h3>50 Most Populated Countries</h3>
        <ResponsiveContainer width="100%" height="100%">
          <BarChart
            width={500}
            height={300}
            data={population}
            margin={{
              top: 5,
              right: 30,
              left: 50,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis
              dataKey="name"
              angle={80}
              textAnchor="start"
              interval={0}
              height={100}
            />
            <YAxis />
            <Tooltip
              formatter={(value: number) =>
                new Intl.NumberFormat("en").format(value)
              }
            />
            <Bar dataKey="population" fill="#8884d8" />
          </BarChart>
        </ResponsiveContainer>

        <h3>Top 25 GHG Emitters (2012)</h3>
        <ResponsiveContainer width="100%" height="100%">
          <PieChart width={600} height={600}>
            <Pie
              dataKey="name"
              isAnimationActive={false}
              data={emissions}
              cx="50%"
              cy="50%"
              outerRadius={80}
              fill="#8884d8"
              label
            />
            <Pie dataKey="emissions" data={emissions} cx={500} cy={200} innerRadius={100} outerRadius={200} fill="#82ca9d" />
            <Tooltip />
          </PieChart>
        </ResponsiveContainer>
        <h3>Article Providers</h3>
        <ResponsiveContainer width="100%" height="100%">
        <RadarChart cx="50%" cy="50%" outerRadius="80%" data={news}>
          <PolarGrid />
          <PolarAngleAxis dataKey="provider" />
          <PolarRadiusAxis />
          <Radar name="Mike" dataKey="count" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
        </RadarChart>
      </ResponsiveContainer>
      </div>
    );
  }
}
