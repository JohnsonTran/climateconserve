import React, { Component } from "react";
import { Row, Col, Image } from "react-bootstrap";
import { NewsObj } from "./types";
import axios from "axios";
import { formatString } from "../utils";

export class NewsPage extends Component<
  {},
  { currArticle: NewsObj | undefined; flag: string | undefined }
> {
  constructor(props = {}) {
    super(props);
    this.state = {
      currArticle: undefined,
      flag: undefined,
    };
  }

  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    const articleId = urlParams.get("id");

    // GET all country data from API
    axios
      .get("https://www.climateconserve.me/api/article/" + articleId)
      .then((res) => {
        this.setState({ currArticle: res.data });
        let cid = res.data.country_id;
        axios
          .get("https://www.climateconserve.me/api/country/" + cid)
          .then((res) => {
            this.setState({ flag: res.data.flag });
          });
      });
  }

  render() {
    const article = this.state.currArticle;
    if (!article) {
      return <h1>Loading...</h1>;
    }

    return (
      <div style={{ textAlign: "center" }}>
        <h1>{article.title}</h1>
        <Image src={article.thumbnail} alt={"Thumbnail Loading..."} fluid />
        <p>Date: {new Date(article.date).toDateString()}</p>
        <p>
          Description: <i>{article.description}</i>
        </p>
        <hr></hr>
        {formatString(article.content).map((paragraph) => {
          return <p>{paragraph}</p>;
        })}
        <p>
          <a href={article.url} target="_blank">
            Read Full Article
          </a>
        </p>
        <hr></hr>

        <Row>
          <Col>
            {" "}
            <p>Language: {article.language}</p>
          </Col>
          <Col>
            <p>Word Count: {article.word_count}</p>
          </Col>
          <Col>
            <p>
              {" "}
              Article ID: <i>{article["article-id"]}</i>
            </p>
          </Col>
        </Row>

        <Row>
          <Col>
            {" "}
            <p>Provider: {article.provider}</p>
          </Col>
          <Col>
            {" "}
            <p>News From: {article.country}</p>
          </Col>
        </Row>

        <Image
          src={this.state.flag}
          alt={"Flag Loading..."}
          style={{ maxWidth: "600px" }}
        />

        <p>
          <a href={`/country?id=${article.country_id}`}>
            Learn about {article.country}
          </a>
        </p>
        <p>
          <a href={`/climate?id=${article.country_id}`}>
            Climate in {article.country}
          </a>
        </p>
      </div>
    );
  }
}
