import React, { Component } from "react";
import axios from "axios";
import { numberWithCommas } from "../utils";
import MaterialTable from "material-table";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { ClimateObj } from "./types";
import "./styles/ClimatePage.css";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export class Climate extends Component<
  {},
  { climates: ClimateObj[] | undefined 
    temp: ClimateObj[] | undefined 
    checkNum: number }
> {
  constructor(props = {}) {
    super(props);
    this.state = {
      climates: undefined,
      temp: undefined,
      checkNum: 0
    };
  }

  componentDidMount() {
    // GET all climate data from API
    axios
      .get("https://www.climateconserve.me/api/climate-reports")
      .then((res) => {
        const data = res.data;
        this.setState({ climates: data });
        this.setState({ temp: data });
      });
  }

  filterValue(checked:boolean, value:number) {
    if (checked) {
      if (value == 1) {
        const filtered = this.state.climates.filter(d => d.name.charCodeAt(0) >= "A".charCodeAt(0) &&
        d.name.charCodeAt(0) <= "J".charCodeAt(0));
        this.setState({ temp: filtered });
      } 
      else if (value == 2) {
        const filtered = this.state.climates.filter(d => d.name.charCodeAt(0) >= "K".charCodeAt(0) &&
        d.name.charCodeAt(0) <= "R".charCodeAt(0));
        this.setState({ temp: filtered });
      }
      else if (value == 3) {
        const filtered = this.state.climates.filter(d => d.name.charCodeAt(0) >= "S".charCodeAt(0) &&
        d.name.charCodeAt(0) <= "Z".charCodeAt(0));
        this.setState({ temp: filtered });
      }
      else if (value == 4) {
        const filtered = this.state.climates.filter(d => parseFloat(d.temperature["2016"]) < 3.0);
        this.setState({ temp: filtered });
      } 
      else if (value == 5) {
        const filtered = this.state.climates.filter(d => parseFloat(d.temperature["2016"]) >= 3.0 && parseFloat(d.temperature["2016"]) < 12.0);
        this.setState({ temp: filtered });
      }
      else if (value == 6) {
        const filtered = this.state.climates.filter(d => parseFloat(d.temperature["2016"]) >= 12.0);
        this.setState({ temp: filtered });
      }

      else if (value == 7) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("Humid continental"));
        this.setState({ temp: filtered });
      }
      else if (value == 8) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("Mediterranean"));
        this.setState({ temp: filtered });
      }
      else if (value == 9) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("Marine west coast"));
        this.setState({ temp: filtered });
      }
      else if (value == 10) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("steppe"));
        this.setState({ temp: filtered });
      }
      else if (value == 11) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("Tropical"));
        this.setState({ temp: filtered });
      }
      else if (value == 12) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("Subtropical") || d.climate_zone.includes("subtropical"));
        this.setState({ temp: filtered });
      }
      else if (value == 13) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("Temperate"));
        this.setState({ temp: filtered });
      }
      else if (value == 14) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("desert"));
        this.setState({ temp: filtered });
      }
      else if (value == 15) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("Subartic"));
        this.setState({ temp: filtered });
      }
      else if (value == 16) {
        const filtered = this.state.climates.filter(d => d.climate_zone.includes("Tundra"));
        this.setState({ temp: filtered });
      }
      else if (value == 17) {
        const filtered = this.state.climates.filter(d => parseFloat(d.rainfall["2016"]) < 50.0);
        this.setState({ temp: filtered });
      }
      else if (value == 18) {
        const filtered = this.state.climates.filter(d => parseFloat(d.rainfall["2016"]) >= 50.0 && parseFloat(d.temperature["2016"]) < 100.0);
        this.setState({ temp: filtered });
      }
      else if (value == 19) {
        const filtered = this.state.climates.filter(d => parseFloat(d.rainfall["2016"]) >= 100.0);
        this.setState({ temp: filtered });
      }
      else if (value == 20) {
        const filtered = this.state.climates.filter(d => parseFloat(d.ghg["2012"]) < 10000.0);
        this.setState({ temp: filtered });
      }
      else if (value == 21) {
        const filtered = this.state.climates.filter(d => parseFloat(d.ghg["2012"]) >= 10000.0 && parseFloat(d.ghg["2012"]) < 100000.0);
        this.setState({ temp: filtered });
      }
      else if (value == 22) {
        const filtered = this.state.climates.filter(d => parseFloat(d.ghg["2012"]) >= 100000.0);
        this.setState({ temp: filtered });
      }
      else if (value == 23) {
        const filtered = this.state.climates.filter(d => parseFloat(d.no2["2012"]) < 1000.0);
        this.setState({ temp: filtered });
      }
      else if (value == 24) {
        const filtered = this.state.climates.filter(d => parseFloat(d.no2["2012"]) >= 1000.0 && parseFloat(d.no2["2012"]) < 25000.0);
        this.setState({ temp: filtered });
      }
      else if (value == 25) {
        const filtered = this.state.climates.filter(d => parseFloat(d.no2["2012"]) >= 25000.0);
        this.setState({ temp: filtered });
      }
      else {
        this.setState({ temp: this.state.climates });
      }
      this.setState({ checkNum: value });
    }
    else {
      this.setState({ checkNum: 0 });
      this.setState({ temp: this.state.climates });
    }
  };

  render() {
    if (!this.state.climates) {
      return <h1>Loading...</h1>;
    }

    return (
      <div>
        <Container>
          <Row>
            <Col>
              <div>
                <h5>Country name:</h5>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkNum == 1}
                      color="primary"
                      onChange={e => this.filterValue(e.target.checked, 1)}
                    />
                  }
                  label="A-J"
                  labelPlacement="end"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkNum == 2}
                      color="primary"
                      onChange={e => this.filterValue(e.target.checked, 2)}
                    />
                  }
                  label="K-R"
                  labelPlacement="end"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkNum == 3}
                      color="primary"
                      onChange={e => this.filterValue(e.target.checked, 3)}
                    />
                  }
                  label="S-Z"
                  labelPlacement="end"
                />
              </div>
            </Col>
            <Col>
              <h5>Temperature:</h5>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 4}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 4)}
                      />
                    }
                    label="Low"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 5}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 5)}
                      />
                    }
                    label="Medium"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 6}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 6)}
                      />
                    }
                    label="High"
                    labelPlacement="end"
                  />
                </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <h5>Rainfall:</h5>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 17}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 17)}
                      />
                    }
                    label="Low Rainfall"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 18}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 18)}
                      />
                    }
                    label="Medium Rainfall"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 19}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 19)}
                      />
                    }
                    label="High Rainfall"
                    labelPlacement="end"
                  />
                </div>
            </Col>
            <Col>
              <h5>Greenhouse Gas Emissions:</h5>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 20}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 20)}
                      />
                    }
                    label="Low GHG Emissions"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 21}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 21)}
                      />
                    }
                    label="Medium GHG Emissions"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 22}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 22)}
                      />
                    }
                    label="High GHG Emissions"
                    labelPlacement="end"
                  />
                </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <h5>Climate Type:</h5>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 7}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 7)}
                      />
                    }
                    label="Humid continental"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 8}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 8)}
                      />
                    }
                    label="Mediterranean"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 9}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 9)}
                      />
                    }
                    label="Marine west coast"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 10}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 10)}
                      />
                    }
                    label="Steppe"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 11}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 11)}
                      />
                    }
                    label="Tropical"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 12}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 12)}
                      />
                    }
                    label="Subtropical"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 13}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 13)}
                      />
                    }
                    label="Temperate"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 14}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 14)}
                      />
                    }
                    label="Desert"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 15}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 15)}
                      />
                    }
                    label="Subartic"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 16}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 16)}
                      />
                    }
                    label="Tundra"
                    labelPlacement="end"
                  />
                </div>
            </Col>
            <Col>
              <h5>NO2 Emissions:</h5>
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 23}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 23)}
                      />
                    }
                    label="Low NO2 Emissions"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 24}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 24)}
                      />
                    }
                    label="Medium NO2 Emissions"
                    labelPlacement="end"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checkNum == 25}
                        color="primary"
                        onChange={e => this.filterValue(e.target.checked, 25)}
                      />
                    }
                    label="High NO2 Emissions"
                    labelPlacement="end"
                  />
                </div>
            </Col>
          </Row>
        </Container>
        <MaterialTable
          columns={[
            {
              title: "ID",
              field: "id",
              type: "numeric",
              align: "left",
              cellStyle: { width: "1%" },
            },
            {
              title: "Country",
              field: "name",
              render: (rowData) => (
                <a href={`climate?id=${rowData.id}`}>{rowData.name}</a>
              ),
            },
            {
              title: "Temperature 2016 (°C)",
              field: "temperature.2016",
              type: "numeric",
            },
            {
              title: "Climate Type",
              field: "climate_zone",
            },
            {
              title: "Rainfall 2016 (mm)",
              field: "rainfall.2016",
              type: "numeric",
            },
            {
              title: "Greenhouse Gas Emissions 2012",
              field: "ghg.2012",
              type: "numeric",
            },
            {
              title: "NO2 2012",
              field: "no2.2012",
              type: "numeric",
            },
          ]}
          data={this.state.temp ? this.state.temp : []}
          title="Climates"
          options={{
            headerStyle: {
              backgroundColor: "#FFC300",
              color: "#FFF",
            },
            pageSize: 10,
          }}
        />
      </div>
    );
  }
}
