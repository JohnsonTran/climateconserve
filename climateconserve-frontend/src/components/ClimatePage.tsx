import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import { ClimateObj, ClimateData } from "./types";
import axios from "axios";
import Plot from "react-plotly.js";
import { numberWithCommas } from "../utils.js";

export class ClimatePage extends Component<
  {},
  {
    currentClimate: ClimateObj | undefined;
    dataReady: boolean;
    ch4: ClimateData | undefined;
    co2: ClimateData | undefined;
    ghg: ClimateData | undefined;
    no2: ClimateData | undefined;
    pm25: ClimateData | undefined;
    rainfall: ClimateData | undefined;
    temperature: ClimateData | undefined;
  }
> {
  constructor(props = {}) {
    super(props);
    this.state = {
      currentClimate: undefined,
      dataReady: false,
      ch4: undefined,
      co2: undefined,
      ghg: undefined,
      no2: undefined,
      pm25: undefined,
      rainfall: undefined,
      temperature: undefined,
    };
  }

  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    const climateId = urlParams.get("id");
    if (climateId) {
      // GET all country data from API
      axios
        .get("https://www.climateconserve.me/api/climate-report/" + climateId)
        .then((res) => {
          const data = res.data;
          this.setState({ currentClimate: data });
          this.fillGraphData();
        });
    }
  }

  fillGraphData() {
    const cc = this.state.currentClimate!;
    let x = [];
    let y = [];
    //ch4
    for (const [key, value] of Object.entries(cc.ch4)) {
      x.push(parseInt(key));
      y.push(parseInt(value));
    }
    this.setState({ ch4: { x: x, y: y } });

    //co2
    x = [];
    y = [];
    for (const [key, value] of Object.entries(cc.co2)) {
      x.push(parseInt(key));
      y.push(parseInt(value));
    }
    this.setState({ co2: { x: x, y: y } });

    //ghg
    x = [];
    y = [];
    for (const [key, value] of Object.entries(cc.ghg)) {
      x.push(parseInt(key));
      y.push(parseInt(value));
    }
    this.setState({ ghg: { x: x, y: y } });

    //no2
    x = [];
    y = [];
    for (const [key, value] of Object.entries(cc.no2)) {
      x.push(parseInt(key));
      y.push(parseInt(value));
    }
    this.setState({ no2: { x: x, y: y } });

    //pm25
    x = [];
    y = [];
    for (const [key, value] of Object.entries(cc.pm25)) {
      x.push(parseInt(key));
      y.push(parseInt(value));
    }
    this.setState({ pm25: { x: x, y: y } });

    //rainfall
    x = [];
    y = [];
    for (const [key, value] of Object.entries(cc.rainfall)) {
      x.push(parseInt(key));
      y.push(parseInt(value));
    }
    this.setState({ rainfall: { x: x, y: y } });

    //temperature
    x = [];
    y = [];
    for (const [key, value] of Object.entries(cc.temperature)) {
      x.push(parseInt(key));
      y.push(parseInt(value));
    }
    this.setState({ temperature: { x: x, y: y } });

    this.setState({ dataReady: true });
  }

  render() {
    const climate = this.state.currentClimate;
    if (!climate) {
      return <h1>Loading...</h1>;
    }
    if (this.state.dataReady) {
      return (
        <div style={{ textAlign: "center" }}>
          <h1>{climate.name}</h1>
          <hr></hr>
          <Row>
            <Col>Air Quality Index: {climate.aqi}</Col>
            <Col>Climate Type: {climate.climate_zone}</Col>
            <Col>PM10: {climate.pm10}</Col>
          </Row>
          <Row>
            <Col>
              <Plot
                data={[
                  {
                    x: this.state.ch4!.x,
                    y: this.state.ch4!.y,
                    type: "scatter",
                    marker: { color: "red" },
                  },
                ]}
                layout={{ title: "CH4 Emissions", width: 500, height: 450 }}
              />
            </Col>
            <Col>
              <Plot
                data={[
                  {
                    x: this.state.co2!.x,
                    y: this.state.co2!.y,
                    type: "scatter",
                    marker: { color: "red" },
                  },
                ]}
                layout={{ title: "CO2 Emissions", width: 500, height: 450 }}
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <Plot
                data={[
                  {
                    x: this.state.ghg!.x,
                    y: this.state.ghg!.y,
                    type: "scatter",
                    marker: { color: "red" },
                  },
                ]}
                layout={{ title: "GHG Emissions", width: 500, height: 450 }}
              />
            </Col>
            <Col>
              <Plot
                data={[
                  {
                    x: this.state.no2!.x,
                    y: this.state.no2!.y,
                    type: "scatter",
                    marker: { color: "red" },
                  },
                ]}
                layout={{ title: "NO2 Emissions", width: 500, height: 450 }}
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <Plot
                data={[
                  {
                    x: this.state.pm25!.x,
                    y: this.state.pm25!.y,
                    type: "scatter",
                    marker: { color: "red" },
                  },
                ]}
                layout={{ title: "PM25 Emissions", width: 500, height: 450 }}
              />
            </Col>
            <Col>
              <Plot
                data={[
                  {
                    x: this.state.rainfall!.x,
                    y: this.state.rainfall!.y,
                    type: "scatter",
                    marker: { color: "red" },
                  },
                ]}
                layout={{ title: "Rainfall", width: 500, height: 450 }}
              />
            </Col>
          </Row>

          <Plot
            data={[
              {
                x: this.state.temperature!.x,
                y: this.state.temperature!.y,
                type: "scatter",
                marker: { color: "red" },
              },
            ]}
            layout={{ title: "Temperature" }}
          />
          <div>
            <p>
              <a href={`/country?id=${climate.id}`}>
                Country of {climate.name}
              </a>
            </p>
            <p>
              <a href={`/article?id=${climate.id}`}>News in {climate.name}</a>
            </p>
          </div>
        </div>
      );
    }
    return (
      <React.Fragment>
        {" "}
        <h1>{climate.name}</h1>
      </React.Fragment>
    );
  }
}
