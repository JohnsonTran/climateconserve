import React from "react";
import ReactDOM from "react-dom";
import {News} from "../components/News";

it("Render Test", ()=> {
    const div = document.createElement("div");
    ReactDOM.render(<News/>, div)
    expect(div.textContent).not.toBe(null);