import React from "react";
import ReactDOM from "react-dom";
import {CountryPage} from "../components/CountryPage";

it("Render Test", ()=> {
    const div = document.createElement("div");
    ReactDOM.render(<CountryPage/>, div)
    expect(div.textContent).not.toBe(null);