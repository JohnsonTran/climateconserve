import React from "react";
import ReactDOM from "react-dom";
import {Home} from "../components/Home";

it("Render Test", ()=> {
    const div = document.createElement("div");
    ReactDOM.render(<Home/>, div)
    expect(div.textContent).not.toBe(null);