import React from "react";
import ReactDOM from "react-dom";
import {Climate} from "../components/Climate";

it("Render Test", ()=> {
    const div = document.createElement("div");
    ReactDOM.render(<Climate/>, div)
    expect(div.textContent).not.toBe(null);
})
