import React from "react";
import ReactDOM from "react-dom";
import {NewsPage} from "../components/NewsPage";

it("Render Test", ()=> {
    const div = document.createElement("div");
    ReactDOM.render(<NewsPage/>, div)
    expect(div.textContent).not.toBe(null);