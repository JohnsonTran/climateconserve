import React from "react";
import ReactDOM from "react-dom";
import {NavBar} from "../components/NavBar";

it("Render Test", ()=> {
    const div = document.createElement("div");
    ReactDOM.render(<NavBar/>, div)
    expect(div.textContent).not.toBe(null);