import React from "react";
import ReactDOM from "react-dom";
import {Countries}from "../components/Countries";

it("Render Test", ()=> {
    const div = document.createElement("div");
    ReactDOM.render(<Countries/>, div)
    expect(div.textContent).not.toBe(null);
})