export function numberWithCommas(x) {
  if (x) return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export function trimText(text) {
  return text.substring(0, 200);
}

// Borrowed from: https://stackoverflow.com/questions/36368491/turning-a-string-into-a-formatted-paragraph-in-javascript
export function formatString(string, numlines = 10) {
  const NO_FORMAT_CAP_IN_CHARS = 1500;
  var length = string.length;
  if (length < NO_FORMAT_CAP_IN_CHARS) {
    return [string];
  }

  var paraLength = Math.round(string.length / numlines);
  var paragraphs = [];
  for (var i = 0; i < numlines; i++) {
    var marker = paraLength;
    //if the marker is right after a space, move marker back one character
    if (string.charAt(marker - 1) == " ") {
      marker--;
    }
    //move marker to end of a word if it's in the middle
    while (string.charAt(marker) != " " && string.charAt(marker) != "") {
      marker++;
    }
    var nextPara = string.substring(0, marker);
    paragraphs.push(nextPara);
    string = string.substring(nextPara.length + 1, string.length);
  }
  return paragraphs;
}
