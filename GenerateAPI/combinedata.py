import json

# combine all the data
# with open('CH4.json', 'r') as ch:
#     ch4 = json.load(ch)

# with open('CO2.json', 'r') as co:
#     co2 = json.load(co)

# with open('GDP.json', 'r') as g:
#     gdp = json.load(g)

# with open('GHG.json', 'r') as gh:
#     ghg = json.load(gh)

# with open('NO2.json', 'r') as no:
#     no2 = json.load(no)

# with open('PM2.5.json', 'r') as pm:
#     pm25 = json.load(pm)

# with open('population.json', 'r') as p:
#     pop = json.load(p)

# with open('region.json', 'r') as r:
#     reg = json.load(r)

# data = {}
# for country in ch4:
#     if country not in reg:
#         # print(country)
#         pass
#     else:
#         if reg[country] != "":
#             data[country] = {"CH4": ch4[country], "CO2": co2[country], "GDP": gdp[country], \
#                         "GHG": ghg[country], "NO2": no2[country], "PM2.5": pm25[country], \
#                         "population": pop[country]["2019"], "region": reg[country]}


# with open("all.json", "w") as outfile:
#     json.dump(data, outfile, indent=4)

# fix the differences in countries
with open('restcountries.json', 'r') as r:
    rc = json.load(r)

with open('worldbankcountry.json', 'r') as w:
    wbc = json.load(w)

with open('newall.json', 'r') as a:
    oldall = json.load(a)

all = {}
names = []
for c in rc:
    names.append(c['name'])

for country in oldall:
    # country_name = country['name']
    wbc_country = next((wbc[i] for i in range(len(wbc)) if wbc[i]['name'] == country), None)
    country_data = oldall[country]
    all[country] = {"capital": wbc_country["capitalCity"], "population": country_data["population"], \
                    "region": country_data["region"], "area": country_data["area"], \
                    "latitude": wbc_country["latitude"], "longitude": wbc_country["longitude"], \
                    "gini": country_data["gini"], "GDP": country_data["GDP"], \
                    "income_level": wbc_country["incomeLevel"]["value"], "flag": country_data["flag"], \
                    "rainfall": country_data["rainfall"], "temperature": country_data["temperature"], \
                    "CH4": country_data["CH4"], "CO2": country_data["CO2"], "GHG": country_data["GHG"], \
                    "NO2": country_data["NO2"], "PM2.5": country_data["PM2.5"]}


with open("all.json", "w") as outfile:
    json.dump(all, outfile, indent=4)