import csv
import json

rain = {}
with open('pr_1991_2016.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    curr_year = 1991
    curr_country = "Afghanistan"
    rain_sum = 0
    for row in reader:
        rainfall = float(row[0].replace(" ", ""))
        year = int(row[1].replace(" ", ""))
        country = row[3][1:]
        if country != curr_country:
            avg_rain = rain_sum / 12
            rain[curr_country]["average_rainfall"][curr_year] = round(avg_rain, 5)
            curr_country = country
            rain_sum = 0
            curr_year = 1991
        if year != curr_year:
            avg_rain = rain_sum / 12
            if curr_country not in rain:
                rain[curr_country] = {"average_rainfall": {}}
            rain[curr_country]["average_rainfall"][curr_year] = round(avg_rain, 5)
            rain_sum = 0
            curr_year = year
        rain_sum += rainfall
        
    avg_rain = rain_sum / 12
    rain[curr_country]["average_rainfall"][curr_year] = round(avg_rain, 5)


with open("rain.json", "w") as outfile:
    json.dump(rain, outfile, indent=4)