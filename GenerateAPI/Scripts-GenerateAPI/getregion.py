import json

f = open("region.txt", "r")

gdp = {}

while True:
    l = f.readline()
    if len(l) == 0:
        break
    line = l.split("\t")
    region = line[0]
    country = line[1]
    gdp[country] = region

with open("/Users/sejsharma/SWEAPI/region.json", "w") as outfile:
    json.dump(gdp, outfile, sort_keys=True, indent=4)