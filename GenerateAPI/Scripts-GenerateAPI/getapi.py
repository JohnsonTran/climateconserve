import json

f = open("pm2.5.txt", "r")
print(f.readline())
gdp = {}

while True:
    l = f.readline()
    if len(l) == 0:
        break
    line = l.split("\t")
    country = line[0]
    length = len(line) - 1
    i = 1
    year = 1990
    country_dict = {}
    while i < length:
        yr_gdp = line[i]
        country_dict[year] = yr_gdp
        i+=1
        year+=1
    gdp[country] = country_dict
    year+=1

with open("/Users/sejsharma/SWEAPI/pm2.5.json", "w") as outfile:
    json.dump(gdp, outfile, sort_keys=True, indent=4)