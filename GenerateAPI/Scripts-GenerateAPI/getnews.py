import requests

URL = "https://rapidapi.p.rapidapi.com/api/search/NewsSearchAPI"
HEADERS = {
    "x-rapidapi-host": "contextualwebsearch-websearch-v1.p.rapidapi.com",
    "x-rapidapi-key": "092100efe0mshb9d2c2829676780p102524jsn56579e99ba8b"
}

news = {}

f = open("countries.txt", "r")
f2 = open("url2.txt", "w")

while True:
    l = f.readline()
    if len(l) == 0:
        break
    line = l.split("\t")
    country = line[0]

    query = country + " climate change"
    page_number = 1
    page_size = 1
    auto_correct = True
    safe_search = False
    with_thumbnails = False
    from_published_date = ""
    to_published_date = ""

    querystring = {"q": query,
                "pageNumber": page_number,
                "pageSize": page_size,
                "autoCorrect": auto_correct,
                "safeSearch": safe_search,
                "withThumbnails": with_thumbnails,
                "fromPublishedDate": from_published_date,
                "toPublishedDate": to_published_date}

    response = requests.get(URL, headers=HEADERS, params=querystring).json()

    for web_page in response["value"]:
        url = web_page["url"]

    news[country] = url
    print(country)
    f2.write(country + "\t" + url + "\n")

with open("/Users/sejsharma/SWEAPI/news.json", "w") as outfile:
    json.dump(news, outfile, sort_keys=True, indent=4)