import json

f = open("news_url.txt", "r")
f2 = open("url2.txt", "r")

news = {}

while True:
    country = f.readline()
    if len(country) == 0:
        break
    url = f.readline().split("\t")[1]
    print(country + " " + url)
    news[country] = url

while True:
    country = f2.readline()
    if len(country) == 0:
        break
    u = f2.readline().split("\t")
    url = u[0]
    if(len(u) > 1):
        url = u[1]
    print(country + " " + url)
    news[country] = url
    
with open("/Users/sejsharma/SWEAPI/news.json", "w") as outfile:
    json.dump(news, outfile, sort_keys=True, indent=4)