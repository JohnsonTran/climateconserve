import csv
import json

temp = {}
with open('tas_1991_2016.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    curr_year = 1991
    curr_country = "Afghanistan"
    temp_sum = 0
    for row in reader:
        temperature = float(row[0].replace(" ", ""))
        year = int(row[1].replace(" ", ""))
        country = row[3][1:]
        if country != curr_country:
            avg_temp = temp_sum / 12
            temp[curr_country]["average_temperature"][curr_year] = round(avg_temp, 2)
            curr_country = country
            temp_sum = 0
            curr_year = 1991
        if year != curr_year:
            avg_temp = temp_sum / 12
            if curr_country not in temp:
                temp[curr_country] = {"average_temperature": {}}
            temp[curr_country]["average_temperature"][curr_year] = round(avg_temp, 2)
            temp_sum = 0
            curr_year = year
        temp_sum += temperature
        
        
    avg_temp = temp_sum / 12
    temp[curr_country]["average_temperature"][curr_year] = round(avg_temp, 5)


with open("temp.json", "w") as outfile:
    json.dump(temp, outfile, indent=4)