import requests
import json
import time

# with open('all.json', 'r') as a:
#     all = json.load(a)

# news = {}
# url = "https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/search/NewsSearchAPI"

# for country in all:
#     querystring = {"q":f"{country} climate change","pageNumber":"1","pageSize":"10","autoCorrect":"true","withThumbnails":"true","fromPublishedDate":"null","toPublishedDate":"null"}

#     headers = {
#         'x-rapidapi-key': "API_KEY REDACTED",
#         'x-rapidapi-host': "contextualwebsearch-websearch-v1.p.rapidapi.com"
#     }

#     response = requests.request("GET", url, headers=headers, params=querystring).json()

#     for story in response["value"]:
#         news[story["id"]] = {"country": country, "url": story["url"], "title": story["title"], "description": story["description"], "content": story["body"], "language": story["language"], "datePublished": story["datePublished"], "provider": story["provider"]["name"], "thumbnail": story["image"]["url"]}


# with open("newnews.json", "w") as outfile:
#     json.dump(news, outfile, indent=4)

# change the ids
# with open('news.json', 'r') as a:
#     all = json.load(a)

# temp = {}
# i = 1
# for news in all:
#     temp[i] = all[news]
#     i += 1

# with open("newnews.json", "w") as outfile:
#     json.dump(temp, outfile, indent=4) 

# with open('all.json', 'r') as a:
#     all = json.load(a)

# newdata = {}
# for country in all:
#     time.sleep(0.5)
#     response = requests.request("GET", "http://climateapi.scottpinkelman.com/api/v1/location/{}/{}".format(all[country]["latitude"], all[country]["longitude"])).json()
#     print(country)
#     try:
#         newdata[country] = {"climate_zone": response["return_values"][0]["zone_description"]}
#     except:
#         newdata[country] = {"climate_zone": None}

# with open("climdata.json", "w") as outfile:
#     json.dump(newdata, outfile, indent=4) 

# with open('all.json', 'r') as a:
#     all = json.load(a)

# with open('climdata.json', 'r') as a:
#     dat = json.load(a)

# data = {}
# for country in all:
#     new = all[country]
#     new["climate_zone"] = dat[country]["climate_zone"]
#     data[country] = new


# with open("newall.json", "w") as outfile:
#     json.dump(data, outfile, indent=4) 

# get word count
# with open('news.json', 'r') as a:
#     all = json.load(a)

# temp = {}
# for news in all:
#     temp[news] = all[news]
#     temp[news]["wordCount"] = len(all[news]["content"].split(" "))

# with open("newnews.json", "w") as outfile:
#     json.dump(temp, outfile, indent=4) 

# with open('org.json', 'r') as a:
#     all = json.load(a)

# colors = ['#FFEC21', '#378AFF', '#FFA32F', '#F54F52', '#93F03B', '#9552EA']
# i = 0

# states = {}
# for org in all:
#     if org["state"] not in states:
#         states[org["state"]] = 1
#     else:
#         states[org["state"]] += 1
# abr = sorted(states)
# for state in abr:
#     print('{name: "' + state + '", value: ' + str(states[state]) + ', fill: "' + colors[i % len(colors)] + '"},')
#     i += 1

with open('school.json', 'r') as a:
    all = json.load(a)

data = {}
for school in all:
    if school["state"] not in data:
        data[school["state"]] = [school["rankPercentile"], 1]
    else:
        temp = data[school["state"]]
        temp[0] += school["rankPercentile"]
        temp[1] += 1
        data[school["state"]] = temp

avg = {}
for state in data:
    avg[state] = data[state][0] / data[state][1]
res = sorted(avg)
for s in res:
    print('{"state": "' + s + '", "avg": ' + str(avg[s]) +  '},')
