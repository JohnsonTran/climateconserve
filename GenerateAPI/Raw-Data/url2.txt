Honduras
	https://exbulletin.com/world/international/811057/
Heavily indebted poor countries (HIPC)
	http://secure.telegraph.co.uk/business/2020/01/24/davos-elites-climate-piety-slammed-luxury-poor-countries-can/
Croatia
	http://www.hillsidefarm.ie/interested-in-climate-change-and-aged-18-25-were-looking-for-you/
Haiti
	https://artistsandclimatechange.com/tag/haiti/
Hungary
	http://euronews.net/2020/02/13/budapest-s-mayor-snubs-orban-by-lobbying-eu-for-direct-access-to-climate-change-cash
IBRD only
	https://nl4worldbank.org/tag/ibrd/
IDA & IBRD total
	https://www.clareshort.org/articles/world-bank-alternative-worse
IDA total
	https://www.brookings.edu/research/inviting-danger-how-federal-disaster-insurance-and-infrastructure-policies-are-magnifying-the-harm-of-climate-change/
IDA blend
	https://www.firstpost.com/india/in-ministry-for-the-future-kim-stanley-robinson-blends-fact-and-fiction-in-a-remarkable-narrative-around-climate-crisis-9438421.html
Indonesia
	https://indianexpress.com/article/opinion/editorials/climate-change-how-infrastructure-can-weather-the-storm-7231125/
IDA only
	https://www.indiewire.com/2019/12/for-sama-ida-documentary-award-honeyland-oscars-1202195360/
Isle of Man
	https://www.manxradio.com/news/isle-of-man-news/isle-of-man-to-join-paris-climate-agreement-this-year/
India
	https://www.hindustantimes.com/india-news/imd-launches-portal-to-aid-climate-change-mitigation-in-india-101616498935980.html
Not classified
	https://globalnews.ca/news/7715671/conservative-party-canada-rob-batherson-climate-change/
Ireland
	https://www.independent.co.uk/climate-change/news/wildfires-uk-risk-climate-crisis-b1821734.html
"Iran, Islamic Rep."
	https://www.natureasia.com/en/nmiddleeast/article/10.1038/nmiddleeast.2016.208
Iraq
	https://www.bostonglobe.com/2021/03/24/world/facing-sweltering-soldiers-flooded-ports-nato-focus-climate-change/
Iceland
	https://www.itv.com/news/2021-03-22/watch-spectacular-drone-footage-of-long-dormant-iceland-volcano-erupting
Israel
	https://www.eater.com/22314197/olive-oil-industry-mediterranean-california-climate-change-impact
Italy
	https://www.eater.com/22314197/olive-oil-industry-mediterranean-california-climate-change-impact
Jamaica
	https://www.newsday.com/news/new-york/jamaica-bay-flooding-climate-change-1.50189562
Jordan
	https://www.cnn.com/2021/03/24/opinions/biodiversity-nature-survival-azoulay-lee-salgar/index.html
Japan
	https://upnewsinfo.com/2021/03/24/climate-change-fight-comes-with-big-trade-offs-for-central-banks-says-report-by-reuters/
Kazakhstan
	https://astanatimes.com/2021/03/kazakhstan-places-62nd-in-ranking-for-global-sustainable-tourism-outstrips-russia-turkey/
Kenya
	https://www.kbc.co.ke/climate-change-worsening-disease-incidence-in-east-africa/
Kyrgyz Republic
	http://isb.nu.edu.pk/Media/NewsDetails?NewsId=42
Cambodia
	https://cambodianewsgazette.com/ncsd-holds-2nd-executive-board-meeting-on-climate-change/
Kiribati
	https://www.einpresswire.com/article/536190803/kiribati-staff-concluding-statement-of-the-2021-article-iv-mission
St. Kitts and Nevis
	https://www.thestkittsnevisobserver.com/st-kitts-and-nevis-take-another-step-toward-reducing-its-carbon-footprint/
"Korea, Rep."
	https://signalscv.com/2021/03/pete-ackerman-fight-climate-change-in-ways-that-help-economy/
Kuwait
	https://menafn.com/1101689792/Zain-highest-ranked-MEA-operator-in-CDP-global-index-on-climate-change?source=302
Latin America & Caribbean (excluding high income)
	https://www.thestkittsnevisobserver.com/bvi-premier-leads-un-eclac-discussions-on-covid-19-recovery/
Lao PDR
	https://reliefweb.int/report/lao-peoples-democratic-republic/disaster-management-reference-handbook-lao-pdr-february-2021
Lebanon
	https://www.wionews.com/world/one-step-away-from-starvation-un-warns-over-30-million-could-die-of-hunger-372844
Liberia
	https://allafrica.com/stories/202103240265.html
Libya
	https://www.anews.com.tr/world/2021/03/19/biden-urges-unsc-to-act-on-ethiopia-syria-yemen-libya-and-myanmar
St. Lucia
	https://oecd-development-matters.org/2021/03/23/the-imfs-turn-on-climate-change/
Latin America & Caribbean
	https://trinidadexpress.com/features/latin-america-caribbean-hold-key-food-security-roles-as-food-systems-summit-approaches/article_92629bce-7d0e-11eb-9c8b-276a2ba5a551.html
Least developed countries: UN classification
	https://allafrica.com/stories/202007090579.html
Low income
	https://news.yahoo.com/low-income-latino-neighborhoods-endure-184641531.html
Liechtenstein
	http://canberratimes.com.au/story/7066978/lets-hear-it-for-the-liechtenstein-led-aussie-recovery
Sri Lanka
	https://www.latimes.com/world-nation/story/2021-03-22/himalayas-climate-change-unsustainable-development
Lower middle income
	https://www.financialexpress.com/opinion/combating-climate-change-make-green-tech-affordable-for-low-and-middle-income-nations
Low & middle income
	https://www.financialexpress.com/opinion/combating-climate-change-make-green-tech-affordable-for-low-and-middle-income-nations
Lesotho
	https://theconversation.com/to-what-extent-does-climate-change-affect-food-insecurity-what-we-found-in-lesotho-156527
Late-demographic dividend
	http://thedailystar.net/business/news/use-demographic-dividend-socio-economic-benefits-2004441
Lithuania
	https://www.politico.eu/article/europe-climate-change-carbon-leakage/
Luxembourg
	https://www2.deloitte.com/lu/en/pages/sustainable-development/articles/climate-related-risks-new-strategy-adaptation-climate-change.html
Latvia
	http://www.baltic-course.com/eng/direct_speech/?doc=152296
"Macao SAR, China"
	https://laotiantimes.com/2021/02/02/sunlight-real-estate-investment-trust-secures-its-sustainability-linked-loan-of-hk500-million-from-dbs-hong-kong/
St. Martin (French part)
	https://www.artnews.com/art-in-america/aia-reviews/the-12th-taipei-biennial-climate-change-globalization-1234586445/
Morocco
	https://www.moroccoworldnews.com/2021/03/337793/exposing-algerias-double-talk-on-moroccos-legitimacy-over-western-sahara/
Monaco
	http://www.bbc.co.uk/weather/features/49780950
Moldova
	https://reliefweb.int/report/moldova/strengthening-moldova-s-disaster-risk-management-and-climate-resilience-facing
Madagascar
	https://countercurrents.org/2021/02/madagascar-a-nation-of-hunger/
Maldives
	https://en.prothomalo.com/bangladesh/bangladesh-maldives-plan-to-sign-pta
Middle East & North Africa
	https://www.brookings.edu/events/the-middle-east-and-the-new-us-administration/
Mexico
	http://rss.cnn.com/~r/rss/cnn_topstories/~3/5e9F-5vc9N0/index.html
Marshall Islands
	https://splash247.com/100-per-tonne-emissions-levy-put-forward-by-the-marshall-islands-and-solomon-islands/
Middle income
	https://www.financialexpress.com/opinion/combating-climate-change-make-green-tech-affordable-for-low-and-middle-income-nations
North Macedonia
	https://www.intellinews.com/eib-to-boost-green-investments-and-support-economic-recovery-of-north-macedonia-205488/
Mali
	https://www.iol.co.za/news/africa/terror-poverty-and-climate-change-in-the-sahel-96721a76-a0b3-4d23-b753-715eb2fb2a87
Malta
	https://timesofmalta.com/articles/view/a-sustainability-task-force-gayle-kimberley.858868
Myanmar
	https://www.thethirdpole.net/en/climate/new-platform-aims-to-reveal-dam-and-climate-impacts-on-the-mekong/
Middle East & North Africa (excluding high income)
	https://www.brookings.edu/opinions/the-middle-east-and-north-africa-and-covid-19-gearing-up-for-the-long-haul/
Montenegro
	http://finchannel.com/world/57137-bosnia-and-herzegovina-montenegro-and-serbia-cooperate-to-strengthen-resilience-to-climate-change
Mongolia
	https://www.staradvertiser.com/2021/03/15/breaking-news/sandstorm-in-china-revives-memories-of-airpocalypses-past/
Northern Mariana Islands
	https://www.guampdn.com/story/news/local/2021/02/12/northern-mariana-islands-extreme-weather-climate-change-report/6714289002/
Mozambique
	https://www.iol.co.za/news/opinion/mozambique-conflict-a-thorn-in-side-of-au-peace-initiatives-7d4f6202-5c80-4fe1-988a-2311e867911f
Mauritania
	https://reliefweb.int/job/3720467/capitalization-and-hea-institutionalization-strategy-5-countries-nigeria-mali-mauritania
Mauritius
	http://nationalpost.com/pmn/news-pmn/disaster-pmn/massive-poisonous-shock-scientists-fear-lasting-impact-from-mauritius-oil-spill
Malawi
	https://hapakenya.com/2021/03/15/ifc-appoints-amena-arif-as-new-head-for-east-africa-malawi/
Malaysia
	https://www.nst.com.my/business/2021/03/676498/nestle-championing-sustainable-future
North America
	https://www.webwire.com/ViewPressRel.asp?aId=271792
Namibia
	https://allafrica.com/stories/202103200353.html
New Caledonia
	http://pacific.scoop.co.nz/2020/10/southwest-pacific-tropical-cyclone-outlook-8/
Niger
	https://www.africanews.com/2021/03/23/nigeria-anne-marie-trevelyan-meets-experts-to-discuss-climate-change-vulnerabilities/
Nigeria
	https://businessday.ng/news/article/fmn-reaffirms-resolve-to-tackle-climate-change-promote-sdgs/
Nicaragua
	https://reliefweb.int/report/nicaragua/hurricanes-nicaragua-left-about-half-million-people-limited-access-drinking-water
Netherlands
	https://www.eater.com/22314200/global-warming-foodborne-illness-lettuce-e-coli
Norway
	https://asc.army.mil/web/news-the-high-profile-high-north/
Nepal
	http://banglamirrornews.com/2021/03/23/nepal-keen-to-export-hydropower-to-bangladesh-momen/
Nauru
	https://www.rnz.co.nz/international/pacific-news/432095/nauru-high-commissioner-to-fiji-michael-aroi-signs-trade-deal-with-us-ambassador-joseph-cella-in-fiji
New Zealand
	https://www.stuff.co.nz/environment/climate-news/124642009/climate-change-calls-for-public-health-expert-to-be-appointed-to-commission.html
OECD members
	https://oecd-development-matters.org/2021/03/23/the-imfs-turn-on-climate-change/
Oman
	https://wattsupwiththat.com/2021/01/05/climate-change-caused-mangrove-collapse-in-oman/
Other small states
	https://www.brookings.edu/research/inviting-danger-how-federal-disaster-insurance-and-infrastructure-policies-are-magnifying-the-harm-of-climate-change/
Pakistan
	https://www.latimes.com/world-nation/story/2021-03-22/himalayas-climate-change-unsustainable-development
Panama
	http://www.thepanamanews.com/2021/03/editorials-tourism-now-and-democrats-abroad-panama-hits-a-restart-button/
Peru
	https://www.environmentalleader.com/2021/03/how-starbucks-plans-to-produce-carbon-neutral-coffee/
Philippines
	https://www.vox.com/2021/3/20/22340607/climate-change-strike-greta-thunberg-fridays-for-future-net-zero-emissions
Palau
	https://nationalinterest.org/feature/trouble-paradise-why-pacific-islands-squabble-matters-washington-177834
Papua New Guinea
	https://asiatimes.com/2021/03/covid-forcing-millions-of-girls-out-of-school-in-se-asia-pacific/
Poland
	https://www.euractiv.com/section/politics/short_news/polands-climate-policy-shows-disappointing-lack-of-ambition/
Pre-demographic dividend
	http://blogs.worldbank.org/africacan/optimism-about-africas-demographic-dividend
Puerto Rico
	https://www.theweeklyjournal.com/business/puerto-rico-could-energize-all-homes-through-renewable-resources-by-2035/article_81368c30-868b-11eb-bca8-1f1f6455caa8.html
"Korea, Dem. People�s Rep."
	https://www.azcentral.com/story/opinion/op-ed/2020/09/16/climate-change-health-crises-victims-deserve-our-love-and-actions/3474094001/
Portugal
	http://bnnbloomberg.ca/bottles-of-wine-under-25-that-taste-as-if-they-cost-twice-that-1.1578750
Paraguay
	https://businessday.ng/news/article/fmn-reaffirms-resolve-to-tackle-climate-change-promote-sdgs/
West Bank and Gaza
	http://www.haaretz.com/world-news/.premium-asylum-seekers-fear-undergoing-coronavirus-testing-red-cross-dg-tells-haaretz-1.8916321
Pacific island small states
	https://www.postguam.com/forum/featured_columnists/a-functioning-pacific-islands-forum-key-to-regional-security/article_00401a3c-8a01-11eb-8663-bb3a9b82dea3.html
Post-demographic dividend
	https://www.hindustantimes.com/brand-post/corporate-entrepreneurship-is-the-way-to-reap-the-demographic-dividend/story-ym9cldzwtvxqcytvomazio.html
French Polynesia
	https://finance.yahoo.com/news/france-hid-impact-french-polynesia-161304870.html
Qatar
	https://menafn.com/1101801960/Qatar-Italian-firms-showcase-agriculture-sustainability-at-AgriteQ?source=290
Romania
	https://news.yahoo.com/romania-greece-aim-speedy-bilateral-205721231.html
Russian Federation
	http://weekly-recorder.com/2018/10/climate-change-to-cause-global-beer-shortage/
Rwanda
	http://www.globaltimes.cn/content/1194361.shtml
South Asia
	http://skynews.com/story/hundreds-of-millions-of-people-could-be-affected-by-heatwaves-within-30-years-study-12254585
Saudi Arabia
	https://humanevents.com/2021/03/24/what-is-behind-the-spike-in-gas-prices/
Sudan
	https://news.yahoo.com/dearth-interpreters-leaves-deaf-students-161129655.html
Senegal
	https://www.project-syndicate.org/commentary/senegal-protests-democracy-economic-recovery-by-abdoulaye-ndiaye-2021-03
Singapore
	https://www.channelnewsasia.com/news/commentary/weather-january-february-monsoon-rain-dry-climate-change-mss-nea-14391482
Solomon Islands
	https://splash247.com/100-per-tonne-emissions-levy-put-forward-by-the-marshall-islands-and-solomon-islands/
Sierra Leone
	https://cleantechnica.com/2020/01/23/cities-bracing-for-climate-change/
El Salvador
	https://angrybearblog.com/2021/03/what-to-do
San Marino
	http://www.eonline.com/news/816556
Somalia
	https://www.onenewspage.com/n/Middle+East/1zn0w59llu/Families-in-Somalia-depend-on-emergency-water-trucking.htm
Serbia
	https://balkangreenenergynews.com/serbia-adopts-bill-on-climate-change/
Sub-Saharan Africa (excluding high income)
	https://www.ippmedia.com/en/news/climate-change-and-conflict-could-fuel-hunger-2020
South Sudan
	https://www.dailymail.co.uk/wires/afp/article-9389079/The-Sahel-Terror-poverty-climate-change.html?ns_mchannel=rss&ns_campaign=1490&ito=1490
Sub-Saharan Africa
	https://www.thefencepost.com/news/nebraska-research-aims-to-improve-irrigated-agriculture-in-sub-saharan-africa/
Small states
	https://www.brookings.edu/research/inviting-danger-how-federal-disaster-insurance-and-infrastructure-policies-are-magnifying-the-harm-of-climate-change/
Sao Tome and Principe
	https://www.thestkittsnevisobserver.com/st-kitts-and-nevis-sign-diplomatic-relations-with-sao-tome-and-principe/
Suriname
	https://www.kaieteurnewsonline.com/2021/03/08/suriname-and-guyana-must-conserve-biodiversity-for-future-generations-suriname-oil-boss/
Slovak Republic
	http://hungarianfreepress.com/2016/04/19/liberal-democratic-values-are-not-self-explanatory-we-must-talk-about-them/
Slovenia
	http://time.com/5815141/slovenia-bees-climate-change/
Sweden
	http://bnnbloomberg.ca/sweden-says-global-warming-could-alter-future-of-monetary-policy-1.1581569
Eswatini
	https://newsdiaryonline.com/au-mourns-departed-eswatini-prime-minister/
Sint Maarten (Dutch part)
	https://matadornetwork.com/read/climate-change-transforming-sint-maarten-place-love/
Seychelles
	https://www.washingtonpost.com/business/why-the-hot-new-shade-for-green-bonds-could-be-blue/2021/03/22/a563c51c-8b51-11eb-a33e-da28941cb9ac_story.html?utm_source=rss&utm_medium=referral&utm_campaign=wp_business
Syrian Arab Republic
	https://reliefweb.int/report/syrian-arab-republic/ten-years-war-ravaged-syrians-grapple-their-worst-hunger-crisis-yet
Turks and Caicos Islands
	https://www.cdema.org/news-centre/news/58-turks-and-caicos-islands
Chad
	https://www.desertsun.com/story/news/environment/2020/07/02/chad-mayes-dives-into-growing-battle-over-joshua-tree-protection/5356281002/
East Asia & Pacific (IDA & IBRD countries)
	https://business.inquirer.net/262571/manila-seen-attracting-fair-share-of-climate-related-investments
Europe & Central Asia (IDA & IBRD countries)
	https://goodmenproject.com/featured-content/central-asias-fruit-and-nut-forests-the-real-garden-of-eden/
Togo
	https://www.eurasiareview.com/07072020-a-landmark-project-aims-at-reforestation-in-togo/
Thailand
	https://www.financialexpress.com/lifestyle/wine-country-shift-south-east-asia-could-be-home-to-the-worlds-best-vineyards-thanks-to-climate-change
Tajikistan
	https://reliefweb.int/report/tajikistan/wfp-and-russian-federation-inaugurate-bakeries-support-school-feeding-tajikistan
Turkmenistan
	https://intellinews.com/satellite-detects-alarming-methane-leaks-after-looking-down-on-turkmenistan-202856/
Latin America & the Caribbean (IDA & IBRD countries)
	https://thecostaricanews.com/plastic-pollution-a-planetary-catastrophe-that-urges-an-international-treaty/
Timor-Leste
	https://www.channelnewsasia.com/news/asia/timor-leste-climate-change-food-security-innovation-12469908?cid=cna_flip_070214
Middle East & North Africa (IDA & IBRD countries)
	https://www.brookings.edu/events/the-middle-east-and-the-new-us-administration/
Tonga
	https://reliefweb.int/report/tonga/gps-nuku-alofa-celebrated-opening-12-renovated-classrooms
South Asia (IDA & IBRD)
	https://ida.worldbank.org/theme/climate?qt-view__theme_tabs__theme_tabs=0
Sub-Saharan Africa (IDA & IBRD countries)
	https://www.thefencepost.com/news/nebraska-research-aims-to-improve-irrigated-agriculture-in-sub-saharan-africa/
Trinidad and Tobago
	http://www.globaltimes.cn/content/1102477.shtml
Tunisia
	http://learningenglish.voanews.com/a/climate-change-puts-north-africa-in-a-hot-spot/5177731.html
Turkey
	https://www.eater.com/22314197/olive-oil-industry-mediterranean-california-climate-change-impact
Tuvalu
	https://www.rnz.co.nz/international/pacific-news/438341/tuvalu-chases-digital-immortality-on-a-blockchain
Tanzania
	https://thenationonlineng.net/don-warns-against-african-swine-fever/
Uganda
	https://exbulletin.com/world/805989/
Ukraine
	http://finchannel.com/world/ukraine/80251-climate-change-challenges-for-ukraine
Upper middle income
	http://bulawayo24.com/opinion/columnist/200336
Uruguay
	https://www.ecowho.com/st/climate+change+Uruguay
United States
	https://harvardmagazine.com/2021/03/making-voters-care-about-climate-change
Uzbekistan
	https://www.intellinews.com/borderlex-eu-edging-closer-to-granting-uzbekistan-gsp-plus-trade-status-205937/
St. Vincent and the Grenadines
	https://news784.com/maduro-proposes-covid-19-vaccine-bank-for-alba-members/
"Venezuela, RB"
	https://ipolitics.ca/2021/03/22/the-drilldown-tories-reject-climate-change-acknowledgment/
British Virgin Islands
	http://royalgazette.com/christopher-famous/article/20200327/for-whom-bell-tolls
Virgin Islands (U.S.)
	https://earther.gizmodo.com/ancient-greenland-cave-sediments-contain-a-climate-chan-1846544689
Vietnam
	https://www.sunnewsonline.com/climate-change-ngo-raises-5000-volunteers-to-plant-trees/
Vanuatu
	https://www.loopvanuatu.com/vanuatu-news/undp-strengthens-development-partnership-vanuatu-government-98475
World
	https://www.zmescience.com/science/climate-change-longer-summer-3627643/
Samoa
	https://www.policyforum.net/has-the-pacific-way-lost-its-way/
Kosovo
	https://businessday.ng/news/article/fmn-reaffirms-resolve-to-tackle-climate-change-promote-sdgs/
"Yemen, Rep."
	https://signalscv.com/2021/03/pete-ackerman-fight-climate-change-in-ways-that-help-economy/
South Africa
	https://www.news24.com/fin24/Companies/Banks/moodys-sounds-alarm-over-sa-banks-climate-change-exposure-through-extensive-mining-lending-20210323
Zambia
	https://www.lusakatimes.com/2021/03/20/north-western-scales-up-tree-planting/
Zimbabwe
    https://www.newsday.co.zw/2021/03/situating-environmental-literature-in-changing-climate/
