echo "Deploying Frontend..."
cd climateconserve-frontend
export REACT_APP_API_URL=/api
npm run build
aws s3 sync build/ s3://climateconserve1-frontend