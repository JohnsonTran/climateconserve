# climateconserve

Link to website: [https://www.climateconserve.me/](https://www.climateconserve.me/)
Git SHA: 6c852148f70195a00f77e517d9301002275ba80d
Pipeline: https://gitlab.com/JohnsonTran/climateconserve/-/pipelines/297318274

## Presentation Video
https://youtu.be/xxKxC-byh2k


## Postman API Documentation

[https://documenter.getpostman.com/view/14757536/TzCTa5SN](https://documenter.getpostman.com/view/14757536/TzCTa5SN)

## Team Info

| Name               | EID     | GitLabID     | Phase Leader |
| ------------------ | ------- | ------------ | ------------ |
| Vivek Chilakamarri | vsc384  | @vchilk1     | 4            |
| Kevin Dao          | kdd994  | @kevidao     | 3            |
| Jeffrey Gordon     | jtg2595 | @tgordon1052 |              |
| Sejal Sharma       | shs2364 | @sharmasej   | 2            |
| Johnson Tran       | jtt2226 | @JohnsonTran | 1            |

## Phase One Time Estimates

| Name               | Estimated | Actual |
| ------------------ | --------- | ------ |
| Vivek Chilakamarri | 12        | 14     |
| Kevin Dao          | 12        | 13     |
| Jeffrey Gordon     | 8         | 6      |
| Sejal Sharma       | 10        | 12     |
| Johnson Tran       | 15        | 17     |

## Phase Two Time Estimates

| Name               | Estimated | Actual |
| ------------------ | --------- | ------ |
| Vivek Chilakamarri | 14        | 20     |
| Kevin Dao          | 15        | 20     |
| Jeffrey Gordon     | 6         | 8      |
| Sejal Sharma       | 16        | 20     |
| Johnson Tran       | 20        | 23     |

## Phase Three Time Estimates

| Name               | Estimated | Actual |
| ------------------ | --------- | ------ |
| Vivek Chilakamarri | 15        | 12     |
| Kevin Dao          | 15        | 12     |
| Jeffrey Gordon     | 6         | 13     |
| Sejal Sharma       | 6         | 6      |
| Johnson Tran       | 15        | 13     |

## Phase Four Time Estimates

| Name               | Estimated | Actual |
| ------------------ | --------- | ------ |
| Vivek Chilakamarri | 5         | 8      |
| Kevin Dao          | 4         | 6      |
| Jeffrey Gordon     | 5         | 6      |
| Sejal Sharma       | 3         | 5      |
| Johnson Tran       | 7         | 10     |
